package va.hagn.bookshop.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import va.hagn.bookshop.data.TestData;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookReviewApiIT {
    @Autowired
    private WebTestClient webClient;

    @Test void emptyBookReview() {
        webClient.post().uri("/api/bookReview")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData("bookId", TestData.cakeSlug))
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.result").isEqualTo(false);
    }
}
