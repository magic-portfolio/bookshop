package va.hagn.bookshop.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BooksRestIT {
    @Autowired
    private WebTestClient webClient;

    @Test void booksByAuthor() {
        webClient.get().uri("/api/books/by-author?author=ance").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.length()").isEqualTo(58);
    }

    @Test void booksByAuthorAll() {
        webClient.get().uri("/api/books/by-author?author=").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.length()").isEqualTo(1000);
    }

    @Test void booksByTitle() {
        webClient.get().uri("/api/books/by-title?title=ower").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.data.length()").isEqualTo(4);
    }

    @Test void cannotSearchAllBooksByTitle() {
        webClient.get().uri("/api/books/by-title?title=").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test void booksBySpaceTitle() {
        webClient.get().uri("/api/books/by-title?title= ").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test void booksByInvalidTitle() {
        webClient.get().uri("/api/books/by-title?title=12341").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test void priceRangeBooks() {
        webClient.get().uri("/api/books/by-price-range?min=90&max=100").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.length()").isEqualTo(13);
    }

    @Test void priceRangeBooksAll() {
        webClient.get().uri("/api/books/by-price-range?min=0&max=1").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.length()").isEqualTo(0);
    }
}
