package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = ContactsController.class)
@Import(Messages.class)
class ContactsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    private final LocaleChecks check = new LocaleChecks();

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/contacts"))
                .andExpect(status().isOk());

        String footerContacts = lookup.english("footer.contacts");

        check.breadCrumbs(actions, footerContacts)
                .andExpect(content().string(containsString(footerContacts + "</h2>")));
    }

    @Test
    void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/contacts?lang=ru"))
                .andExpect(status().isOk());

        String footerContacts = lookup.russian("footer.contacts");

        check.breadCrumbs(actions, footerContacts)
                .andExpect(content().string(containsString(footerContacts + "</h2>")));
    }
}
