package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CookieUtilTest {
    @Test
    public void addNewSlug() {
        CookieUtil util = new CookieUtil();
        String result = util.addSlug("asdf", "aaa/bbb");
        assertEquals("aaa/bbb/asdf", result);
    }

    @Test
    public void addExistingSlug() {
        CookieUtil util = new CookieUtil();
        String result = util.addSlug("aaa", "aaa/bbb");
        assertEquals("aaa/bbb", result);
    }

    @Test
    public void removeSlug() {
        CookieUtil util = new CookieUtil();
        String result = util.removeSlug("aaa", "aaa/bbb");
        assertEquals("bbb", result);
    }

    @Test
    public void slugRatingIsZeroByDefault() {
        CookieUtil util = new CookieUtil();
        assertEquals(0, util.getSlugRating("asdf", "fdsa"));
    }

    @Test
    public void slugRatingWhenSet() {
        CookieUtil util = new CookieUtil();
        assertEquals(4, util.getSlugRating("asdf", "fdsa:3/asdf:4/zzz:1"));
    }

    @Test
    public void addInitialSlugRating() {
        CookieUtil util = new CookieUtil();
        assertEquals("asdf:4", util.changeSlugRating("asdf", 4, (String) null));
        assertEquals("asdf:4", util.changeSlugRating("asdf", 4, ""));
    }

    @Test
    public void addSlugRatingToNonEmptySet() {
        CookieUtil util = new CookieUtil();
        assertEquals("fdsa:3/zzz:1/asdf:4", util.changeSlugRating("asdf", 4, "fdsa:3/zzz:1"));
    }

    @Test
    public void updateSlugRating() {
        CookieUtil util = new CookieUtil();
        assertEquals("fdsa:3/zzz:1/asdf:4", util.changeSlugRating("asdf", 4, "fdsa:3/asdf:2/zzz:1"));
    }
}