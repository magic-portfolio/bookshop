package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import java.util.List;

import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = SearchController.class)
@Import(Messages.class)
class SearchControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    private BookService bookService;

    private final LocaleChecks check = new LocaleChecks();

    @BeforeEach
    void stubBookService() {
        when(bookService.getPageOfSearchResultBooks(any(), any(), any()))
                .thenReturn(new PageImpl<>(TestData.threeBooks));
    }

    @Test
    void englishL18n() throws Exception {
        Book one = new Book("Et arcu");
        Book two = new Book("Dolor tempus");
        Book three = new Book("Aliquam gravida");

        when(bookService.getBooksData()).thenReturn(List.of(one, two, three));

        ResultActions actions = mockMvc.perform(get("/search/asdf"))
                .andExpect(status().isOk())
                .andExpect(content().string(stringContainsInOrder(one.getTitle(), two.getTitle(), three.getTitle())));
        check.title(actions, lookup.english("searchbar.button"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/search/asdf?lang=ru"))
                .andExpect(status().isOk());
        check.title(actions, lookup.russian("searchbar.button"));
    }
}
