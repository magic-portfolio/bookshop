package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.genre.GenreService;
import va.hagn.bookshop.genre.GenresPageController;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GenresPageController.class)
@Import(Messages.class)
class GenresControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    private GenreService genreService;

    private final LocaleChecks check = new LocaleChecks();

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/genres"))
                .andExpect(status().isOk());
        check.header(actions, lookup.english("header.genres"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/genres?lang=ru"))
                .andExpect(status().isOk());
        check.header(actions, lookup.russian("header.genres"));
    }
}
