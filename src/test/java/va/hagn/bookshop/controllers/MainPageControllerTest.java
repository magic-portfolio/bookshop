package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.TagService;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MainPageController.class)
@Import(Messages.class)
class MainPageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    private BookService bookService;
    @MockBean
    private TagService tagService;

    private final LocaleChecks check = new LocaleChecks();

    @BeforeEach void stubBookService() {
        when(bookService.getBooksData()).thenReturn(TestData.threeBooks);
        when(bookService.getPageOfRecommendedBooks(any(), any())).thenReturn(new PageImpl<>(TestData.threeBooks));
        when(bookService.getPageOfRecentBooks(any(), any())).thenReturn(Page.empty());
        when(bookService.getPageOfPopularBooks(any(), any())).thenReturn(Page.empty());
    }

    @Test void showRecommendedBooks() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attributeExists("recommendedBooks"))
                .andExpect(content().string(stringContainsInOrder(TestData.one.getTitle(), TestData.two.getTitle(), TestData.three.getTitle())))
        ;
    }

    @Test void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/?lang=en"))
                .andExpect(status().isOk());

        check.mainHeader(actions, lookup.english("header.main"));
        check.searchBar(actions,
                lookup.english("searchbar.placeholder"), lookup.english("searchbar.button"));

        actions
                .andExpect(content().string(containsString(lookup.english("main.title") + "</title>")))
                .andExpect(content().string(containsString(lookup.english("main.recommended"))))
                .andExpect(content().string(containsString(lookup.english("header.recent"))))
                .andExpect(content().string(containsString(lookup.english("header.popular"))))
        ;
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/?lang=ru"))
                .andExpect(status().isOk());

        check.mainHeader(actions, lookup.russian("header.main"));
        check.searchBar(actions,
                lookup.russian("searchbar.placeholder"), lookup.russian("searchbar.button"));

        actions
                .andExpect(content().string(containsString(lookup.russian("main.title") + "</title>")))
                .andExpect(content().string(containsString(lookup.russian("main.recommended"))))
                .andExpect(content().string(containsString(lookup.russian("header.recent"))))
                .andExpect(content().string(containsString(lookup.russian("header.popular"))))
        ;
    }
}
