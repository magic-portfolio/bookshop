package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RecentPageController.class)
@Import(Messages.class)
class RecentBooksControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    private BookService bookService;

    private final LocaleChecks check = new LocaleChecks();

    @BeforeEach void stubBookService() {
        when(bookService.getPageOfRecentBooksOrderedByDate(any(), any(), any(), any()))
                .thenReturn(new PageImpl<>(TestData.threeBooks));
    }

    @Test void showBooks() throws Exception {

        mockMvc.perform(get("/books/recent"))
                .andExpect(status().isOk())
                .andExpect(view().name("books/recent"))
                .andExpect(model().attributeExists("booksList"))
                .andExpect(content().string(stringContainsInOrder(TestData.one.getTitle(), TestData.two.getTitle(), TestData.three.getTitle())))
        ;
    }

    @Test void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/recent?lang=en"))
                .andExpect(status().isOk());
        check.breadCrumbs(actions, lookup.english("header.recent"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/recent?lang=ru"))
                .andExpect(status().isOk());
        check.breadCrumbs(actions, lookup.russian("header.recent"));
    }
}
