package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.data.BookRepository;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = BookshopPostponedController.class)
@Import({Messages.class, CookieUtil.class})
class PostponedBooksControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    private final LocaleChecks check = new LocaleChecks();

    @MockBean
    private BookRepository bookRepository;

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/postponed"))
                .andExpect(status().isOk());

        check.title(actions, lookup.english("header.postponed"));
    }

    @Test
    void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/postponed?lang=ru"))
                .andExpect(status().isOk());

        check.title(actions, lookup.russian("header.postponed"));
    }
}
