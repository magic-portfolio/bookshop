package va.hagn.bookshop.controllers;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import va.hagn.bookshop.data.*;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = BooksController.class)
@Import({Messages.class, CookieUtil.class})
class BooksControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    BookService bookService;
    @MockBean
    BookReviewService bookReviewService;
    @MockBean
    ResourceStorage resourceStorage;

    private final LocaleChecks check = new LocaleChecks();

    @BeforeEach
    public void stub() {
        when(bookService.getRatingVotesBySlug(anyString())).thenReturn(new RatingVotes(1, 2, 3, 4, 5));
        when(bookService.getBookBySlug(anyString())).thenReturn(TestData.hordes);
    }

    @Test void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/" + TestData.cakeSlug))
                .andExpect(status().isOk());
        check.title(actions, "Hordes");
        actions
                .andExpect(content().string(containsString(lookup.english("header.author") + ":")));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/books/" + TestData.cakeSlug + "?lang=ru"))
                .andExpect(status().isOk());
        check.title(actions, "Hordes");
        actions
                .andExpect(content().string(containsString(lookup.russian("header.author") + ":")));
    }

    @Test void numberOfReviews() throws Exception {
        var w = new Review();
        when(bookReviewService.getReviewsBySlug(anyString(), anyString())).thenReturn(List.of(w, w, w, w, w));

        mockMvc.perform(get("/books/" + TestData.cakeSlug))
                .andExpect(status().isOk())
                .andExpect(bodyContainsString("Comment-content", 5));
    }

    private static ResultMatcher bodyContainsString(String substring, int times) {
        return r -> assertEquals(times, StringUtils.countMatches(r.getResponse().getContentAsString(), substring));
    }
}
