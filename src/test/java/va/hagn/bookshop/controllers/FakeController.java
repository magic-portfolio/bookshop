package va.hagn.bookshop.controllers;

import org.springframework.stereotype.Controller;

/** It is for making certain unit tests independent from the db layer */
@Controller
public class FakeController {
}
