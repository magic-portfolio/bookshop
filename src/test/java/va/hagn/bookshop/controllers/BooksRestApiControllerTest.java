package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.TestData;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BooksRestApiController.class)
class BooksRestApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Test void booksByAuthor() throws Exception {
        when(bookService.getBooksByAuthor(anyString())).thenReturn(TestData.threeBooks);

        mockMvc.perform(get("/api/books/by-author?author=vasya").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
        ;
    }

    @Test void booksByTitle() throws Exception {
        when(bookService.getBooksByTitle(anyString())).thenReturn(TestData.threeBooks);

        mockMvc.perform(get("/api/books/by-title?title=pupkin").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(3)))
        ;
    }

    @Test void priceRangeBooks() throws Exception {
        when(bookService.getBooksWithPriceBetween(anyInt(), anyInt())).thenReturn(TestData.threeBooks);

        mockMvc.perform(get("/api/books/by-price-range?min=3&max=9").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
        ;
    }
}
