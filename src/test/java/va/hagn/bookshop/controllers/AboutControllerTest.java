package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = AboutController.class)
@Import(Messages.class)
class AboutControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    private final LocaleChecks check = new LocaleChecks();

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/about"))
                .andExpect(status().isOk());
        check.breadCrumbs(actions, lookup.english("footer.about"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/about?lang=ru"))
                .andExpect(status().isOk());
        check.breadCrumbs(actions, lookup.russian("footer.about"));
    }
}
