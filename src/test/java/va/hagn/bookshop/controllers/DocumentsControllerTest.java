package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = DocumentsController.class)
@Import(Messages.class)
class DocumentsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    private final LocaleChecks check = new LocaleChecks();

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/documents"))
                .andExpect(status().isOk());
        check.header(actions, lookup.english("footer.documents"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/documents?lang=ru"))
                .andExpect(status().isOk());
        check.header(actions, lookup.russian("footer.documents"));
    }
}
