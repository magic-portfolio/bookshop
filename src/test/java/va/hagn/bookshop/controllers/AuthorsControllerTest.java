package va.hagn.bookshop.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import va.hagn.bookshop.author.AuthorService;
import va.hagn.bookshop.author.AuthorsController;
import va.hagn.bookshop.data.Author;
import va.hagn.bookshop.locale.LocaleChecks;
import va.hagn.bookshop.locale.Messages;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuthorsController.class)
@Import(Messages.class)
class AuthorsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Messages lookup;

    @MockBean
    private AuthorService authorService;

    private final LocaleChecks check = new LocaleChecks();

    @Test void showAuthors() throws Exception {
        Author one = new Author("Blbul", "Hazaran");
        Author two = new Author("Narek", "Khorenazi");
        Author three = new Author("Hazar", "Unhaxt");
        Author four = new Author("Meer", "Zoray");

        String firstLetter = "BUKVARAZ";
        String secondLetter = "ZvaBuka";
        when(authorService.getAuthorsGroupedByLetter()).thenReturn(
                Map.of(firstLetter, List.of(one, two), secondLetter, List.of(three, four)));

        mockMvc.perform(get("/authors"))
                .andExpect(status().isOk())
                .andExpect(view().name("authors/index"))
                .andExpect(model().attributeExists("authorsMap"))
                .andExpect(content().string(stringContainsInOrder(firstLetter, one.getLastName(), two.getLastName())))
                .andExpect(content().string(stringContainsInOrder(secondLetter, three.getLastName(), four.getLastName())));
    }

    @Test
    void englishL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/authors"))
                .andExpect(status().isOk());
        check.header(actions, lookup.english("header.authors"));
    }

    @Test void russianL18n() throws Exception {
        ResultActions actions = mockMvc.perform(get("/authors?lang=ru"))
                .andExpect(status().isOk());
        check.header(actions, lookup.russian("header.authors"));
    }
}
