package va.hagn.bookshop;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import va.hagn.bookshop.controllers.FakeController;

@WebMvcTest(FakeController.class)
class BookshopApplicationTests {

    @Test void loadMavenDependencies() {
    }
}
