package va.hagn.bookshop.locale;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import va.hagn.bookshop.controllers.FakeController;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(FakeController.class)
@Import(Messages.class)
class MessagesTest {

    @Autowired
    private Messages lookup;

    @Test void english() {
        assertEquals("Genres", lookup.english("header.genres"));
    }

    @Test void russian() {
        assertEquals("Жанры", lookup.russian("header.genres"));
    }

}
