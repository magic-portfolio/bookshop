package va.hagn.bookshop.locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.ResourceBundle;

@Component
public class Messages {

    public static final Locale LOCALE_RUSSIAN = new Locale("ru");

    @Value("${spring.messages.basename}")
    private String basename;

    public String lookupMessage(String messageKey, Locale locale) {
        return ResourceBundle.getBundle(basename, locale)
                .getString(messageKey);
    }

    public String english(String messageKey) {
        return lookupMessage(messageKey, Locale.ENGLISH);
    }

    public String russian(String messageKey) {
        return lookupMessage(messageKey, LOCALE_RUSSIAN);
    }
}
