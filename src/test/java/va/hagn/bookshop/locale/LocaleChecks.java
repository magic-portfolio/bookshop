package va.hagn.bookshop.locale;

import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class LocaleChecks {
    public LocaleChecks() {
    }

    public ResultActions breadCrumbs(ResultActions actions, String text) throws Exception {
        return header(actions, text)
                .andExpect(content().string(containsString(text + "</span>")))
                ;
    }

    public ResultActions header(ResultActions actions, String text) throws Exception {
        return title(actions, text)
                .andExpect(content().string(containsString(text + "</a>")))
                ;
    }

    public ResultActions title(ResultActions actions, String text) throws Exception {
        return actions
                .andExpect(content().string(containsString(text + "</title>")))
                .andExpect(content().string(containsString(text + "</h1>")))
                .andExpect(content().string(not(containsString("??"))));
    }

    public ResultActions mainHeader(ResultActions actions, String text) throws Exception {
        return actions
                .andExpect(content().string(containsString(text + "</a>")))
                .andExpect(content().string(not(containsString("??"))));
    }

    public ResultActions searchBar(ResultActions actions, String placeholder, String button) throws Exception {
        return actions
                .andExpect(content().string(containsString(placeholder)))
                .andExpect(content().string(containsString(button)));
    }
}
