package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.BookPage;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class BookReviewIT {
    private WebDriver driver;

    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void rateBook() {
        var book = new BookPage(driver)
                .open(baseUrl, TestData.cakeSlug);

        int rating = book.totalRatings();
        book.rate(3)
            .waitRatingSuccessMessage();

        book.open(baseUrl, TestData.cakeSlug);
        assertEquals(rating + 1, book.totalRatings());
    }

    @Test void reviewBook() {
        var book = new BookPage(driver)
                .open(baseUrl, TestData.cakeSlug);

        int totalReviews = book.totalReviews();

        int rating = book.incrementUserRating();
        book.waitRatingSuccessMessage();

        String randomPhrase = TestData.randomThreeWords();
        book.typeReview(randomPhrase)
                .submitReview()
                .waitReviewSuccessMessage();

        book.open(baseUrl, TestData.cakeSlug);

        assertEquals(totalReviews + 1, book.totalReviews());
        book.validateReview(rating, randomPhrase);
    }

    @Test void switchReviewLikes() {
        var book = new BookPage(driver)
                .open(baseUrl, TestData.cakeSlug);

        String randomPhrase = TestData.randomThreeWords();
        book.typeReview(randomPhrase)
                .submitReview()
                .waitReviewSuccessMessage()
                .open(baseUrl, TestData.cakeSlug);

        int reviewId = book.findReviewContaining(randomPhrase);
        int likes = book.likesCount(reviewId);
        book.clickOnLike(reviewId)
                .waitLikesCount(reviewId, likes + 1);
        book.checkLikeIsHighlighted(reviewId);

        int dislikes = book.dislikesCount(reviewId);
        book.clickOnDislike(reviewId)
                .waitDislikesCount(reviewId, dislikes + 1);
        assertEquals(likes, book.likesCount(reviewId));
        book.checkDislikeIsHighlighted(reviewId);

        book.open(baseUrl, TestData.cakeSlug);
        assertAll(
                () -> assertEquals(likes, book.likesCount(reviewId), "likes"),
                () -> assertEquals(dislikes + 1, book.dislikesCount(reviewId), "dislikes"),
                () -> book.checkDislikeIsHighlighted(reviewId)
        );
    }
}
