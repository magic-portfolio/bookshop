package va.hagn.bookshop.ui;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Stream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
class ClickThroughIT {
    private static WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    private static MainPage page;

    /** cannot make it BeforeAll because baseUrl cannot be made static */
    @BeforeEach
    void openMainPage() {
        if (page == null)
            page = new MainPage(driver).open(baseUrl);
    }

    @ParameterizedTest(name = "{1}")
    @MethodSource
    void headerClicker(Method method, String description) throws Exception {
        Page newPage = (Page) method.invoke(page.header());
        newPage.waitUntilLoaded()
                .checkHighlightedTabs();
    }

    private static Stream<Arguments> headerClicker() {
        return clickMethods(HeaderFragment.class.getDeclaredMethods());
    }

    @ParameterizedTest(name = "{1}")
    @MethodSource
    void footerClicker(Method method, String description) throws Exception {
        Page newPage = (Page) method.invoke(page.footer());
        newPage.waitUntilLoaded()
                .checkHighlightedTabs();
    }

    private static Stream<Arguments> footerClicker() {
        return clickMethods(FooterFragment.class.getDeclaredMethods());
    }

    private static Stream<Arguments> clickMethods(Method[] declaredMethods) {
        return Arrays.stream(declaredMethods)
                .filter(m -> Modifier.isPublic(m.getModifiers()))
                .filter(m -> m.getName().startsWith("clickOn"))
                .map(m -> Arguments.of(m, m.getName()));
    }

    @Test void clickOnTag() {
        MainPage mainPage = new MainPage(driver).open(baseUrl);
        BooksPoolPage tagPage = mainPage.clickOnFirstLargeTag();
        tagPage.waitUntilLoaded();
    }

    @Test void clickOnGenre() {
        GenresPage parentPage = new GenresPage(driver).open(baseUrl);
        BooksPoolPage genrePage = parentPage.clickOnFirstGenre();
        genrePage.waitUntilLoaded();
    }
}
