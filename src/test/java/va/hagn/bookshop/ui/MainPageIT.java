package va.hagn.bookshop.ui;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.CardsFragment;
import va.hagn.bookshop.ui.page.MainPage;
import va.hagn.bookshop.ui.page.StripFragment;

import java.util.function.Function;
import java.util.stream.Stream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class MainPageIT {
    private static WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    private static MainPage mainPage;

    /** Before each because baseUrl cannot be made static. */
    @BeforeEach void openMainPage() {
        if (mainPage == null)
            mainPage = new MainPage(driver).open(baseUrl);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource
    void scrollingStripRight(String name, Function<MainPage, StripFragment> stripGetter) {
        StripFragment strip = stripGetter.apply(mainPage);
        CardsFragment cards = strip.cards();

        int previousCount = cards.count();
        strip.clickOnRightArrow();
        cards.waitForCardsCountToBeMoreThan(previousCount);

        cards.assertBookAuthorsAreDisplayed();
    }

    public static Stream<Arguments> scrollingStripRight() {
        return Stream.of(
                Arguments.of("recommended", (Function<MainPage, StripFragment>) MainPage::recommendedStrip),
                Arguments.of("recent", (Function<MainPage, StripFragment>) MainPage::recentStrip),
                Arguments.of("popular", (Function<MainPage, StripFragment>) MainPage::popularStrip)
        );
    }
}
