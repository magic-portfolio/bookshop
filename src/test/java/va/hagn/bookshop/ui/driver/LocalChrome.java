package va.hagn.bookshop.ui.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Profile("local-chrome")
public class LocalChrome implements WebDriverCreator {
    @Value("${webdriver.chrome.driver}")
    private String driverPath;

    @Value("${webdriver.download.path}")
    private String downloadPath;

    @Override
    public WebDriver create() {
        // https://github.com/SeleniumHQ/selenium/issues/11750
        // https://www.selenium.dev/blog/2022/using-java11-httpclient/
        System.setProperty("webdriver.http.factory", "jdk-http-client");

        var service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(driverPath))
                .build();

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", DownloadConfig.chromePrefs(downloadPath));

        return new ChromeDriver(service, options);
    }
}
