package va.hagn.bookshop.ui.driver;

import org.openqa.selenium.WebDriver;

public interface WebDriverCreator {
    WebDriver create();
}
