package va.hagn.bookshop.ui.driver;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ExtendWith(WebDriverExtension.class)
public class WebDriverExtensionStaticIT {

    private static WebDriver driver;

    @BeforeAll static void driverMustBeSetAhead() {
        assert driver != null;
    }

    @BeforeEach void driverMustBeSet() {
        assert driver != null;
    }

    @ParameterizedTest
    @CsvSource({"data:,chrome://version/", "chrome:,chrome://apps/"})
    void browserIsSharedBetweenTests(String prefix, String url) {
        assertThat(driver.getCurrentUrl()).startsWith(prefix);
        driver.get(url);
    }
}
