package va.hagn.bookshop.ui.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Profile("local-firefox")
public class LocalFirefox implements WebDriverCreator {
    @Value("${webdriver.firefox.driver}")
    private String driverPath;

    @Value("${webdriver.firefox.binary}")
    private String binaryPath;

    @Value("${webdriver.download.path}")
    private String downloadPath;

    @Override
    public WebDriver create() {
        var service = new GeckoDriverService.Builder()
                .usingDriverExecutable(new File(driverPath))
                .build();

        var options = new FirefoxOptions()
                .setBinary(binaryPath)
                .setProfile(DownloadConfig.ffProfile(downloadPath));

        return new FirefoxDriver(service, options);
    }
}
