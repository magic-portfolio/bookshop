package va.hagn.bookshop.ui.driver;

import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.HashMap;
import java.util.Map;

/**
 * Browser profiles that specify the location when downloading files buy clicking on a link,
 * e.g. pdf and epub files of the books.
 * <a href="https://www.browserstack.com/guide/download-file-using-selenium-python">See also</a>
 */
final class DownloadConfig {
    static FirefoxProfile ffProfile(String downloadPath) {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", downloadPath);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", String.join(";",
                "text/csv",
                "text/plain",
                "application/pdf",
                "application/octet-stream",
                "application/epub+zip"));
        return profile;
    }

    /**
     * <a href="https://github.com/aerokube/selenoid/issues/730#issuecomment-968405085">selenoid download path</a>
     */
    static Map<String, Object> chromePrefs(String downloadPath) {
        Map<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("profile.default_content_setting_values.automatic_downloads", 1);
        chromePrefs.put("download.prompt_for_download", false);
        chromePrefs.put("download.directory_upgrade", true);
        chromePrefs.put("download.default_directory", downloadPath);
        chromePrefs.put("savefile.default_directory", downloadPath);
        return chromePrefs;
    }
}
