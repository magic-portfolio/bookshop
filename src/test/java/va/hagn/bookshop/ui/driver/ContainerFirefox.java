package va.hagn.bookshop.ui.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
@Profile("container-firefox")
public class ContainerFirefox implements WebDriverCreator {
    @Value("${webdriver.container.url}")
    private String driverUrl;

    @Value("${webdriver.download.path}")
    private String downloadPath;

    @Override
    public WebDriver create() {
        try {
            FirefoxOptions options = new FirefoxOptions();
            options.setProfile(DownloadConfig.ffProfile(downloadPath));

            RemoteWebDriver driver = new RemoteWebDriver(new URL(driverUrl), options);
            driver.setFileDetector(new LocalFileDetector());  // enables image upload from localhost

            return driver;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
