package va.hagn.bookshop.ui.driver;

import org.junit.jupiter.api.extension.*;
import org.openqa.selenium.WebDriver;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Optional;

public class WebDriverExtension implements TestInstancePostProcessor, BeforeAllCallback, AfterAllCallback, AfterEachCallback {

    private WebDriverCreator creator;
    private WebDriver driver;
    private boolean driverIsStatic;

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        creator = SpringExtension.getApplicationContext(context).getBean(WebDriverCreator.class);
        findStaticWebDriverField(context.getRequiredTestClass()).ifPresent(this::injectStaticDriver);
    }

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext context) throws Exception {
        findInstanceWebDriverField(testInstance.getClass()).ifPresent(f -> injectInstanceDriver(testInstance, f));
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        if (!driverIsStatic && driver != null) {
            driver.quit();
            driver = null;
        }
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    private void injectInstanceDriver(Object testInstance, Field f) {
        createDriverIfRequired();
        injectDriver(f, testInstance);
    }

    private void injectStaticDriver(Field f) {
        driverIsStatic = true;
        createDriverIfRequired();
        injectDriver(f, null);
    }

    private void createDriverIfRequired() {
        if (driver == null) {
            driver = creator.create();
        }
    }

    private void injectDriver(Field f, Object instance) {
        f.setAccessible(true);
        try {
            f.set(instance, driver);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static Optional<Field> findStaticWebDriverField(Class<?> testClass) {
        return Arrays.stream(testClass.getDeclaredFields())
                .filter(WebDriverExtension::isWebDriver)
                .filter(WebDriverExtension::isStatic)
                .findFirst();
    }

    private static Optional<Field> findInstanceWebDriverField(Class<?> testClass) {
        return Arrays.stream(testClass.getDeclaredFields())
                .filter(WebDriverExtension::isWebDriver)
                .filter(f -> !isStatic(f))
                .findFirst();
    }

    private static boolean isStatic(Field f) {
        return f != null && Modifier.isStatic(f.getModifiers());
    }

    private static boolean isWebDriver(Field f) {
        return f != null && WebDriver.class == f.getType();
    }
}
