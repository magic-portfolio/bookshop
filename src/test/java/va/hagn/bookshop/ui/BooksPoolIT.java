package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.*;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class BooksPoolIT {
    private static WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    static CardsFragment testMoreBooks(BooksPoolPage page) {
        page.checkHighlightedTabs();

        CardsFragment cards = page.cards();

        int previousCount = cards.count();
        page.showMore();
        cards.waitForCardsCountToBeMoreThan(previousCount);

        cards.assertBookAuthorsAreDisplayed();

        return cards;
    }

    @Test void tagPage() {
        var page = new TagPage(driver).open(baseUrl, "18");
        testMoreBooks(page);
    }

    @Test void genrePage() {
        var page = new GenrePage(driver).open(baseUrl, "genre-gba-119");
        testMoreBooks(page);
    }

    @Test void popularPage() {
        var page = new PopularPage(driver).open(baseUrl);
        testMoreBooks(page);
    }

    @Test void searchPage() {
        String query = "eat";

        var page = new SearchPage(driver).open(baseUrl, query);
        CardsFragment cards = page.cards();
        cards.waitUntilLoaded();
        assertThat(cards.count()).isEqualTo(20);

        testMoreBooks(page);

        assertThat(cards.count()).isEqualTo(32);
        assertCardsTitleContainsQuery(cards.cards(), query);
    }

    private void assertCardsTitleContainsQuery(List<CardFragment> cards, String query) {
        for (CardFragment card : cards) {
            assertThat(card.title()).contains(query);
        }
    }

    @Test void recentPage() {
        var page = new RecentPage(driver)
                .open(baseUrl)
                .changeFromDate("01.01.2023");
        testMoreBooks(page);
    }

    @Test void authorBooksPage() {
        var page = new AuthorBooksPage(driver).open(baseUrl, "author-wgw-765");
        var cards = testMoreBooks(page);
        assertCardsHaveAuthor(cards.cards(), "Awcoate Glendon");
    }

    private void assertCardsHaveAuthor(List<CardFragment> cards, String author) {
        for (CardFragment card : cards) {
            assertThat(card.author()).isEqualTo(author);
        }
    }
}
