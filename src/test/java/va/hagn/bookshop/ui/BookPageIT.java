package va.hagn.bookshop.ui;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.BookPage;
import va.hagn.bookshop.ui.page.MainPage;

import java.io.File;
import java.io.IOException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class BookPageIT {
    private static WebDriver driver;

    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Value("${browser.download.mounted-path}")
    private String downloadPath;


    @Test void clickOnMainPage() {
        var mainPage = new MainPage(driver).open(baseUrl);

        BookPage bookPage = mainPage.recommendedStrip().clickOnFirstBook();
        bookPage.waitUntilLoaded();
    }

    @Test void uploadBookImage() {
        var book = new BookPage(driver)
                .open(baseUrl, TestData.slaughterhouseSlug)
                .uploadImage("test-book-cover.png");
        book.waitUntilLoaded();
        book.checkImageIsDisplayed();
    }

    @BeforeEach void deleteDownloadFolder() throws IOException {
        FileUtils.cleanDirectory(new File(downloadPath));
    }

    @AfterEach void deleteDownloadFolderAgain() throws IOException {
        FileUtils.cleanDirectory(new File(downloadPath));
    }


    @Test void downloadBook() {
        var book = new BookPage(driver)
                .open(baseUrl, TestData.wakeSlug);
        book.checkDownloadPopupIsHidden();
        book.clickOnDownload();
        book.waitForDownloadPopup();
        book.clickOnEpub();
        book.waitForFileToDownload(downloadPath, "Wake.epub");
    }
}
