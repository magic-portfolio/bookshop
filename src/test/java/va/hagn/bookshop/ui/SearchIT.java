package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.MainPage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class SearchIT {
    private static WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void emptySearch() {
        var mainPage = new MainPage(driver).open(baseUrl);
        mainPage.header().searchStrip().clickOnSearch();
        mainPage.waitUntilLoaded();
        mainPage.checkErrorIsDisplayed();
    }
}
