package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.BookPage;
import va.hagn.bookshop.ui.page.PostponedPage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class PostponedPageIT {
    private static WebDriver driver;

    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void addTwoBooksToPostponedAndRemoveThem() {
        var book = new BookPage(driver);

        book.open(baseUrl, TestData.flodderSlug)
                .addToPostponed()
                .waitUntilLoaded();

        book.open(baseUrl, TestData.wakeSlug)
                .addToPostponed()
                .waitUntilLoaded();

        var postponed = new PostponedPage(driver)
                .open(baseUrl)
                .waitForBooksCount(2);

        postponed
                .removeFirstBook()
                .waitForBooksCount(1)

                .removeFirstBook()
                .waitForBooksCount(0);
    }
}
