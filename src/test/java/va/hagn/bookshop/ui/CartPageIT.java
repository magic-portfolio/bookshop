package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.data.TestData;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.BookPage;
import va.hagn.bookshop.ui.page.CartPage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class CartPageIT {
    private static WebDriver driver;

    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void addTwoBooksToCartAndRemoveThem() {
        var book = new BookPage(driver);

        book.open(baseUrl, TestData.slaughterhouseSlug)
                .addToCart()
                .waitUntilLoaded();

        book.open(baseUrl, TestData.wakeSlug)
                .addToCart()
                .waitUntilLoaded();

        CartPage cart = new CartPage(driver)
                .open(baseUrl)
                .waitForBooksCount(2);

        cart
                .removeFirstBook()
                .waitForBooksCount(1)

                .removeFirstBook()
                .waitForBooksCount(0);
    }
}
