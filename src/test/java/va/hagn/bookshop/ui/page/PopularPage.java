package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class PopularPage extends BooksPoolPage {

    public PopularPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public PopularPage open(String baseUrl) {
        super.open(baseUrl + "/books/popular");
        return this;
    }

    @Override
    public PopularPage checkHighlightedTabs() {
        header().assertPopularTabIsHighlighted();
        return this;
    }
}
