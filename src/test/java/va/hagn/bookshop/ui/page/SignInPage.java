package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SignInPage extends Page {
    private final By button = By.id("sendauth");

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public SignInPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForm();
        return this;
    }

    private void waitForm() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(button));
    }

    public SignInPage navigateByClickingOn(By selector) {
        defaultWaiter()
                .ignoring(ElementClickInterceptedException.class)
                .until(webDriver -> {
                    webDriver.findElement(selector).click();
                    return webDriver.getCurrentUrl().contains("signin");
                });
        return this;
    }
}
