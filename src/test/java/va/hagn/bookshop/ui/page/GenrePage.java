package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class GenrePage extends BooksPoolPage {

    public GenrePage(WebDriver driver) {
        super(driver);
    }

    public GenrePage open(String baseUrl, String slug) {
        super.open(baseUrl + "/genres/" + slug);
        return this;
    }
}
