package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CartPage extends Page {
    private final By total = By.cssSelector(".Cart-block_total");
    protected final By bookRow = By.className("Cart-product");
    protected final By removeButton = By.cssSelector("button[data-sendstatus='UNLINK']");

    public CartPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public CartPage open(String baseUrl) {
        super.open(baseUrl + "/books/cart");
        return this;
    }

    @Override
    public CartPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForTotal();
        return this;
    }

    private void waitForTotal() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(total));
    }

    public CartPage waitForBooksCount(int count) {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBe(bookRow, count));
        return this;
    }

    public CartPage removeFirstBook() {
        driver.findElement(removeButton).click();
        return this;
    }
}
