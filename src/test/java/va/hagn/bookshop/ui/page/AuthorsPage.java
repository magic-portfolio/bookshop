package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AuthorsPage extends Page {
    private final By authorLink = By.className("Authors-item");
    private final By letterLink = By.className("Authors-link");
    private final By authorWithMaxBooks = By.xpath("//a[contains(text(), 'Awcoate')]");

    public AuthorsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public AuthorsPage open(String baseUrl) {
        super.open(baseUrl + "/authors");
        return this;
    }

    @Override
    public AuthorsPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForAuthorLinks();
        waitForLetterLinks();
        return this;
    }

    public void waitForAuthorLinks() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(authorLink, 5));
    }

    public void waitForLetterLinks() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(letterLink, 5));

        defaultWaiter()
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(letterLink));
    }

    @Override
    public AuthorsPage checkHighlightedTabs() {
        header().assertAuthorsTabIsHighlighted();
        return this;
    }

    public AuthorPage clickOnFirstAuthor() {
        driver.findElement(authorLink).click();
        return new AuthorPage(driver);
    }

    public AuthorPage clickOnAuthorWithMaxBooks() {
        driver.findElement(authorWithMaxBooks).click();
        return new AuthorPage(driver);
    }
}
