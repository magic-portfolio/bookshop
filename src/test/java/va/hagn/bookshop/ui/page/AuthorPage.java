package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AuthorPage extends Page {
    protected final StripFragment bookStrip;
    protected final By showAllBooks = By.className("Author-books");

    public AuthorPage(WebDriver driver) {
        super(driver);
        bookStrip = new StripFragment(driver, "author");
    }

    public AuthorPage open(String baseUrl, String slug) {
        super.open(baseUrl + "/authors/" + slug);
        return this;
    }

    @Override
    public AuthorPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForButton();
        return this;
    }

    private void waitForButton() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(showAllBooks));
    }

    public AuthorPage waitForStripToLoad() {
        bookStrip.waitUntilLoaded();
        return this;
    }

    public StripFragment bookStrip() {
        return bookStrip;
    }

    public AuthorBooksPage showAllBooks() {
        driver.findElement(showAllBooks).click();
        return new AuthorBooksPage(driver);
    }
}
