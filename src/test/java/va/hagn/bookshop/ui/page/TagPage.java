package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class TagPage extends BooksPoolPage {

    public TagPage(WebDriver driver) {
        super(driver);
    }

    public TagPage open(String baseUrl, String tagId) {
        super.open(baseUrl + "/tags/" + tagId);
        return this;
    }
}
