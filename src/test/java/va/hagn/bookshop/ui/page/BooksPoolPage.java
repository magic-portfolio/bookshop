package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BooksPoolPage extends Page {
    protected final CardsFragment cardsFragment;
    protected final By showMoreButton = By.className("btn");

    public BooksPoolPage(WebDriver driver) {
        super(driver);
        cardsFragment = new CardsFragment(driver);
    }

    @Override
    public BooksPoolPage waitUntilLoaded() {
        super.waitUntilLoaded();
        cardsFragment.waitUntilLoaded();
        return this;
    }

    public BooksPoolPage showMore() {
        clickOn(showMoreButton);
        return this;
    }

    public CardsFragment cards() {
        return cardsFragment;
    }
}
