package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PostponedPage extends Page {
    protected final By buyAllButton = By.className("Cart-buyAll");
    private final By total = By.cssSelector(".Cart-block_total");
    protected final By bookRow = By.className("Cart-product");
    protected final By removeButton = By.cssSelector("button[data-sendstatus='UNLINK']");

    public PostponedPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public PostponedPage open(String baseUrl) {
        super.open(baseUrl + "/books/postponed");
        return this;
    }

    @Override
    public PostponedPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForBuyButton();
        return this;
    }

    private void waitForBuyButton() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(buyAllButton));
    }

    public PostponedPage waitForBooksCount(int count) {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBe(bookRow, count));
        return this;
    }

    public PostponedPage removeFirstBook() {
        driver.findElement(removeButton).click();
        return this;
    }
}
