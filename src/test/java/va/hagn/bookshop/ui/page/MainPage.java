package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.assertj.core.api.Assertions.assertThat;

public class MainPage extends Page {
    protected final StripFragment recommendedFragment;
    protected final StripFragment recentFragment;
    protected final StripFragment popularFragment;

    private final By tags = By.className("Tag");
    private final By largeTags = By.className("Tag_lg");

    public MainPage(WebDriver driver) {
        super(driver);
        recommendedFragment = new StripFragment(driver, "recommended");
        recentFragment = new StripFragment(driver, "recent");
        popularFragment = new StripFragment(driver, "popular");
    }

    @Override
    public MainPage open(String baseUrl) {
        super.open(baseUrl);
        checkHighlightedTabs();
        return this;
    }

    @Override
    public MainPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForSectionHeaders();
        recommendedFragment.waitUntilLoaded();
        recentFragment.waitUntilLoaded();
        popularFragment.waitUntilLoaded();
        waitForTags();
        return this;
    }

    private void waitForSectionHeaders() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBe(By.className("Section-title"), 3));
    }

    private void waitForTags() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(largeTags, 4));
    }

    @Override
    public MainPage checkHighlightedTabs() {
        header().assertMainTabIsHighlighted();
        return this;
    }

    public StripFragment recommendedStrip() {
        return recommendedFragment;
    }

    public StripFragment recentStrip() {
        return recentFragment;
    }

    public StripFragment popularStrip() {
        return popularFragment;
    }

    public BooksPoolPage clickOnFirstLargeTag() {
        driver.findElement(largeTags).click();
        return new BooksPoolPage(driver);
    }

    public MainPage checkErrorIsDisplayed() {
        var errorElement = driver.findElement(By.id("errorMessage"));
        assertThat(errorElement.getText()).isNotEmpty();
        return this;
    }
}
