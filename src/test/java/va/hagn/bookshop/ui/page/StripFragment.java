package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StripFragment extends Fragment {
    protected final String dataLoad;
    protected final By parentDiv;
    protected final By rightArrow;
    protected final By cards;
    protected final CardsFragment cardsFragment;

    public StripFragment(WebDriver driver, String dataLoad) {
        super(driver);
        this.dataLoad = dataLoad;
        parentDiv = By.xpath(String.format(".//div[@data-load='%s']", dataLoad));
        rightArrow = By.xpath(String.format(".//div[@data-load='%s']/..//button[contains(@class,'slick-next')]", dataLoad));
        cards = By.xpath(String.format(".//div[@data-load='%s']//div[@class='Card']", dataLoad));
        cardsFragment = new CardsFragment(driver, cards);
    }

    @Override
    public StripFragment waitUntilLoaded() {
        cardsFragment.waitUntilLoaded();
        return this;
    }

    public StripFragment clickOnRightArrow() {
        clickOn(rightArrow);
        return this;
    }

    public CardsFragment cards() {
        return cardsFragment;
    }

    public BookPage clickOnFirstBook() {
        cards().clickOnFirstBook();
        return new BookPage(driver);
    }
}
