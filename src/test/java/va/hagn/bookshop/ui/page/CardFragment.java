package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CardFragment extends Fragment {
    private final WebElement element;
    public CardFragment(WebDriver driver, WebElement element) {
        super(driver);
        this.element = element;
    }

    public String author() {
        return element.findElement(By.className("Card-description")).getAttribute("textContent").trim();
    }

    public String title() {
        return element.findElement(By.className("Card-title")).getAttribute("textContent").trim();
    }
}
