package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ContactsPage extends Page {
    private final By sendButton = By.id("sendMessage");

    public ContactsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public ContactsPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForForm();
        return this;
    }

    private void waitForForm() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(sendButton));
    }
}
