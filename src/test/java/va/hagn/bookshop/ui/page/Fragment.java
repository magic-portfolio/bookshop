package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Fragment {
    protected final WebDriver driver;

    public Fragment(WebDriver driver) {
        this.driver = driver;
    }

    public Fragment clickOn(By selector) {
        driver.findElement(selector).click();
        return this;
    }

    public Fragment waitUntilLoaded() {
        return this;
    }

    public FluentWait<WebDriver> defaultWaiter() {
        return new WebDriverWait(driver, Duration.ofSeconds(10));
    }
}
