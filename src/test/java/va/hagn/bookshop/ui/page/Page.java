package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class Page extends Fragment {
    protected final HeaderFragment headerFragment;
    protected final FooterFragment footerFragment;

    public Page(WebDriver driver) {
        super(driver);
        headerFragment = new HeaderFragment(driver);
        footerFragment = new FooterFragment(driver);
    }

    public HeaderFragment header() {
        return headerFragment;
    }

    public FooterFragment footer() {
        return footerFragment;
    }

    @Override
    public Page waitUntilLoaded() {
        header().waitUntilLoaded();
        footer().waitUntilLoaded();
        return this;
    }

    public Page checkHighlightedTabs() {
        header().assertNoTabIsHighlighted();
        return this;
    }

    public Page open(String baseUrl) {
        driver.get(baseUrl);
        waitUntilLoaded();
        return this;
    }
}
