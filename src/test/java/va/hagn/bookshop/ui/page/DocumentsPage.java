package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DocumentsPage extends Page {
    private final By sectionImage = By.className("Documents-image");

    public DocumentsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public DocumentsPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForImages();
        return this;
    }

    private void waitForImages() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(sectionImage, 1));
    }
}
