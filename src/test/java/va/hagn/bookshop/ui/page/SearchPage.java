package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class SearchPage extends BooksPoolPage {

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public SearchPage open(String baseUrl, String query) {
        super.open(baseUrl + "/search/" + query);
        return this;
    }

    @Override
    public SearchPage waitUntilLoaded() {
        header().waitUntilLoaded();
        return this;
    }
}
