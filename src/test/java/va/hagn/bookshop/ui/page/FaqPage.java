package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class FaqPage extends Page {
    private final By faqItem = By.cssSelector(".HideBlock");

    public FaqPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public FaqPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForItemsToLoad();
        return this;
    }

    private void waitForItemsToLoad() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(faqItem, 2));
    }
}
