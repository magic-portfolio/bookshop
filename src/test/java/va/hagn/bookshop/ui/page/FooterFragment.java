package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class FooterFragment extends Fragment {
    protected final By logo = By.cssSelector(".Footer .logo-text");
    protected final String menu = ".//ul[contains(@class, 'Footer-menu')]";
    protected final By homePageLink =  By.xpath(menu + "//a[@href='/']");
    protected final By genresLink = By.xpath(menu + "//a[contains(@href, 'genres')]");
    protected final By recentLink = By.xpath(menu + "//a[contains(@href, 'recent')]");
    protected final By popularLink = By.xpath(menu + "//a[contains(@href, 'popular')]");
    protected final By authorsLink = By.xpath(menu + "//a[contains(@href, 'authors')]");
    protected final By aboutLink = By.xpath(menu + "//a[contains(@href, 'about')]");
    protected final By contactsLink = By.xpath(menu + "//a[contains(@href, 'contacts')]");
    protected final By documentsLink = By.xpath(menu + "//a[contains(@href, 'documents')]");
    protected final By faqLink = By.xpath(menu + "//a[contains(@href, 'faq')]");
    protected final By signInLink = By.xpath(menu + "//a[contains(@href, 'signin')]");
    protected final By languageSelector = By.id("locales");

    public FooterFragment(WebDriver driver) {
        super(driver);
    }

    @Override
    public FooterFragment waitUntilLoaded() {
        waitForLanguageSelector();
        return this;
    }

    private void waitForLanguageSelector() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(languageSelector));
    }

    public MainPage clickOnHomePage() {
        clickOn(homePageLink);
        return new MainPage(driver);
    }

    public GenresPage clickOnGenres() {
        clickOn(genresLink);
        return new GenresPage(driver);
    }

    public RecentPage clickOnRecent() {
        clickOn(recentLink);
        return new RecentPage(driver);
    }

    public PopularPage clickOnPopular() {
        clickOn(popularLink);
        return new PopularPage(driver);
    }

    public AuthorsPage clickOnAuthors() {
        clickOn(authorsLink);
        return new AuthorsPage(driver);
    }

    public MainPage clickOnLogo() {
        clickOn(logo);
        return new MainPage(driver);
    }

    public AboutPage clickOnAbout() {
        clickOn(aboutLink);
        return new AboutPage(driver);
    }

    public ContactsPage clickOnContacts() {
        clickOn(contactsLink);
        return new ContactsPage(driver);
    }

    public DocumentsPage clickOnDocuments() {
        clickOn(documentsLink);
        return new DocumentsPage(driver);
    }

    public FaqPage clickOnFaq() {
        clickOn(faqLink);
        return new FaqPage(driver);
    }

    public SignInPage clickOnSignIn() {
        return new SignInPage(driver)
                .navigateByClickingOn(signInLink);
    }

    public FooterFragment switchToRussianLocale() {
        Select lang = new Select(driver.findElement(languageSelector));
        lang.selectByValue("ru");
        return this;
    }
}
