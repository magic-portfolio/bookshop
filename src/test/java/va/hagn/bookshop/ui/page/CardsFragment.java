package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CardsFragment extends Fragment {
    protected final By cards;
    public CardsFragment(WebDriver driver, By cards) {
        super(driver);
        this.cards = cards;
    }

    public CardsFragment(WebDriver driver) {
        this(driver, By.cssSelector(".Cards .Card"));
    }

    @Override
    public CardsFragment waitUntilLoaded() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(cards, 4));
        return this;
    }

    public List<CardFragment> cards() {
        return asFragmentList(driver.findElements(cards));
    }

    public int count() {
        return cards().size();
    }

    public CardsFragment waitForCardsCountToBeMoreThan(int count) {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(cards, count));
        return this;
    }

    private List<CardFragment> asFragmentList(List<WebElement> elements) {
        return elements.stream().map(c -> new CardFragment(driver, c)).toList();
    }

    public void assertBookAuthorsAreDisplayed() {
        for (CardFragment card : cards()) {
            String author = card.author();
            assertThat(author)
                    .as("check that the book '%s' has an author", card.title())
                    .isNotBlank();
            assertThat(author)
                    .as("check that the book '%s' has an author", card.title())
                    .doesNotStartWith("undef");
        }
    }

    public CardsFragment clickOnFirstBook() {
        driver.findElement(cards).click();
        return this;
    }
}
