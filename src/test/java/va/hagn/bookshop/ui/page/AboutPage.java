package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AboutPage extends Page {
    private final By image = By.cssSelector(".pict img");

    public AboutPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public AboutPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForImage();
        return this;
    }

    private void waitForImage() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(image));
    }
}
