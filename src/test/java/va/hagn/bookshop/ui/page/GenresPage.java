package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GenresPage extends Page {
    private final By tagElement = By.className("Tag");

    public GenresPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public GenresPage waitUntilLoaded() {
        super.waitUntilLoaded();
        waitForTags();
        return this;
    }

    @Override
    public GenresPage open(String baseUrl) {
        super.open(baseUrl + "/genres");
        return this;
    }

    private void waitForTags() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(tagElement, 10));

        defaultWaiter()
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(tagElement));
    }

    @Override
    public GenresPage checkHighlightedTabs() {
        header().assertGenresTabIsHighlighted();
        return this;
    }

    public BooksPoolPage clickOnFirstGenre() {
        driver.findElement(tagElement).click();
        return new BooksPoolPage(driver);
    }
}
