package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RecentPage extends BooksPoolPage {

    public RecentPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public RecentPage open(String baseUrl) {
        super.open(baseUrl + "/books/recent");
        return this;
    }

    @Override
    public RecentPage checkHighlightedTabs() {
        header().assertRecentTabIsHighlighted();
        return this;
    }

    public RecentPage changeFromDate(String date) {
        WebElement fromDate = driver.findElement(By.id("fromdaterecent"));
        fromDate.sendKeys(Keys.chord(Keys.CONTROL, "a")); // select all text
        fromDate.sendKeys(date);
        fromDate.sendKeys(Keys.TAB);

        return this;
    }
}
