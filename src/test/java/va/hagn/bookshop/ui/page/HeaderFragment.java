package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.assertj.core.api.Assertions.assertThat;

public class HeaderFragment extends Fragment {
    protected final By logo = By.cssSelector(".Header .logo-text");
    protected final By tabItem = By.cssSelector("header .menu-item");
    protected final By activeTab = By.className("menu-item_ACTIVE");
    protected final String menu = ".//div[@class='Header-menu']";
    protected final By homePageLink =  By.xpath(menu + "//a[@href='/']");
    protected final By genresLink = By.xpath(menu + "//a[contains(@href, 'genres')]");
    protected final By recentLink = By.xpath(menu + "//a[contains(@href, 'recent')]");
    protected final By popularLink = By.xpath(menu + "//a[contains(@href, 'popular')]");
    protected final By authorsLink = By.xpath(menu + "//a[contains(@href, 'authors')]");
    protected final String rowMain = ".//div[@class='Header-rowMain']";
    protected final By postponedLink = By.xpath(rowMain + "//a[contains(@href, 'postponed')]");
    protected final By cartLink = By.xpath(rowMain + "//a[contains(@href, 'cart')]");
    protected final By signInLink = By.xpath(rowMain + "//a[contains(@href, 'signin')]");

    private final SearchFragment searchFragment;

    public HeaderFragment(WebDriver driver) {
        super(driver);
        searchFragment = new SearchFragment(driver);
    }

    public SearchFragment searchStrip() {
        return searchFragment;
    }

    @Override
    public Fragment waitUntilLoaded() {
        searchStrip().waitForSearchField();
        waitForTabs();
        return this;
    }

    private void waitForTabs() {
        defaultWaiter()
                .until(ExpectedConditions.numberOfElementsToBe(tabItem, 5));
    }

    public MainPage clickOnLogo() {
        clickOn(logo);
        return new MainPage(driver);
    }

    private void assertSingleTabIsActive() {
        assertThat(driver.findElements(activeTab)).hasSize(1);
    }

    public void assertNoTabIsHighlighted() {
        assertThat(driver.findElements(activeTab)).hasSize(0);
    }

    protected void assertTabIsActive(By tabSelector) {
        assertSingleTabIsActive();
        assertParentElementIsActive(tabSelector);
    }

    private void assertParentElementIsActive(By tabSelector) {
        WebElement listItem = driver.findElement(tabSelector).findElement(By.xpath(".."));
        assertThat(listItem.getAttribute("class")).contains("ACTIVE");
    }

    public MainPage clickOnHomePage() {
        clickOn(homePageLink);
        return new MainPage(driver);
    }

    public HeaderFragment assertMainTabIsHighlighted() {
        assertTabIsActive(homePageLink);
        return this;
    }

    public GenresPage clickOnGenres() {
        clickOn(genresLink);
        return new GenresPage(driver);
    }

    public HeaderFragment assertGenresTabIsHighlighted() {
        assertTabIsActive(genresLink);
        return this;
    }

    public RecentPage clickOnRecent() {
        clickOn(recentLink);
        return new RecentPage(driver);
    }

    public HeaderFragment assertRecentTabIsHighlighted() {
        assertTabIsActive(recentLink);
        return this;
    }

    public PopularPage clickOnPopular() {
        clickOn(popularLink);
        return new PopularPage(driver);
    }

    public HeaderFragment assertPopularTabIsHighlighted() {
        assertTabIsActive(popularLink);
        return this;
    }

    public AuthorsPage clickOnAuthors() {
        clickOn(authorsLink);
        return new AuthorsPage(driver);
    }

    public HeaderFragment assertAuthorsTabIsHighlighted() {
        assertTabIsActive(authorsLink);
        return this;
    }

    public CartPage clickOnCart() {
        clickOn(cartLink);
        return new CartPage(driver);
    }

    public PostponedPage clickOnPostponed() {
        clickOn(postponedLink);
        return new PostponedPage(driver);
    }

    public SignInPage clickOnSignIn() {
        clickOn(signInLink);
        return new SignInPage(driver);
    }

    public String getGenresTabLabel() {
        return driver.findElement(genresLink).getAttribute("textContent");
    }
}
