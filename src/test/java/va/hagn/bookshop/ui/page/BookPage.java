package va.hagn.bookshop.ui.page;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class BookPage extends Page {

    protected final By imageUploadInput = By.id("dialog");
    protected final By bookImage = By.cssSelector("#uploadButton img");
    protected final By downloadPopup = By.id("myPopup");
    protected final By downloadButton = By.cssSelector(".ProductCard-cartElement img[alt='download.svg']");
    protected final By epubLink = By.xpath(".//div[@id='myPopup']//a[contains(text(), '.epub')]");
    protected final By addToCartButton = By.cssSelector("button[data-sendstatus='CART']");
    protected final By addToPostponedButton = By.cssSelector("button[data-sendstatus='KEPT']");
    protected final By assessmentTitle = By.cssSelector(".ProductCard-assessment .Rating-title");
    protected final By ratingTitle = By.cssSelector(".ProductCard-rating .Rating-title");
    protected final By reviewArea = By.id("review");
    protected final By sendReviewButton = By.cssSelector(".Comments-addComment button");
    protected final By successSendComment = By.className("Comments-successSend");
    protected final By reviewsCount = By.className("Product-comments-amount");

    public BookPage(WebDriver driver) {
        super(driver);
    }

    public BookPage open(String baseUrl, String slug) {
        super.open(baseUrl + "/books/" + slug);
        return this;
    }

    public BookPage uploadImage(String fileName) {
        String absolutePath = new File("src/test/resources", fileName).getAbsolutePath();
        driver.findElement(imageUploadInput).sendKeys(absolutePath);
        return this;
    }

    public BookPage checkImageIsDisplayed() {
        assertThat(driver.findElement(bookImage).getAttribute("naturalWidth"))
                .as("Image is displayed")
                .isNotEqualTo("0");
        return this;
    }

    public BookPage checkDownloadPopupIsHidden() {
        assertThat(driver.findElement(downloadPopup).isDisplayed()).isFalse();
        return this;
    }

    public BookPage waitForDownloadPopup() {
        defaultWaiter().until(ExpectedConditions.visibilityOfElementLocated(downloadPopup));
        return this;
    }

    public BookPage clickOnDownload() {
        clickOn(downloadButton);
        return this;
    }

    public BookPage clickOnEpub() {
        clickOn(epubLink);
        return this;
    }

    public BookPage waitForFileToDownload(String folder, String fileName) {
        var dir = new File(folder);
        defaultWaiter()
                .until(d -> folderContainsFile(dir, fileName));
        return this;
    }

    private static boolean folderContainsFile(File folder, String fileName) {
        File[] files = folder.listFiles((d, name) -> Objects.equals(name, fileName));
        return files != null && files.length > 0;
    }

    public BookPage addToCart() {
        driver.findElement(addToCartButton).click();
        return this;
    }

    public BookPage addToPostponed() {
        driver.findElement(addToPostponedButton).click();
        return this;
    }

    public BookPage rate(int rating) {
        driver.findElement(ratingStarSelector(rating)).click();
        return this;
    }

    private static By ratingStarSelector(int rating) {
        return By.cssSelector(String.format("input[name='rating'][value='%d']", rating));
    }

    public BookPage waitRatingSuccessMessage() {
        WebElement title = defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(assessmentTitle));

        String css = title.getAttribute("class");
        assertThat(css).contains("success");

        return this;
    }

    public int totalRatings() {
        String rating = driver.findElement(ratingTitle).getText();
        return Integer.parseInt(rating.substring(1, rating.length() - 1));
    }

    public BookPage typeReview(String review) {
        driver.findElement(reviewArea).sendKeys(review);
        return this;
    }

    public BookPage submitReview() {
        driver.findElement(sendReviewButton).click();
        return this;
    }

    public BookPage waitReviewSuccessMessage() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(successSendComment));
        return this;
    }

    public int totalReviews() {
        String total = driver.findElement(reviewsCount).getText();
        return Integer.parseInt(total.split(" ")[0]);
    }

    public int incrementUserRating() {
        int newRating = currentUserRating() % 5 + 1;

        rate(newRating);

        return newRating;
    }

    public int currentUserRating() {
        var elements = driver.findElements(By.cssSelector(".Rating_input input:checked"));
        if (elements.isEmpty())
            return 0;
        else
            return Integer.parseInt(elements.get(0).getAttribute("value"));
    }

    public void validateReview(int rating, String phrase) {
        WebElement comment = findCommentByText(phrase).orElseThrow();
        var softly = new SoftAssertions();
        softly.assertThat(reviewUser(comment)).as("Review user").isEqualTo("Guest");
        softly.assertThat(reviewRating(comment)).as("Review rating").isEqualTo(rating);
        softly.assertThat(reviewDateTime(comment)).as("Review post date").isNotBlank();
        softly.assertThat(reviewText(comment)).as("Review text").isEqualTo(phrase);
        softly.assertAll();
    }

    private String reviewUser(WebElement comment) {
        return comment.findElement(By.cssSelector(".Comment-title > span:first-child")).getText();
    }

    private int reviewRating(WebElement comment) {
        return comment.findElements(By.cssSelector(".Rating-star_view")).size();
    }

    private String reviewText(WebElement comment) {
        return comment.findElement(By.cssSelector(".Spoiler-visible")).getText();
    }

    private String reviewDateTime(WebElement comment) {
        return comment.findElement(By.cssSelector(".Comment-date")).getText();
    }

    public int likesCount(int reviewId) {
        String likeText = driver.findElement(likeCountSelector(reviewId)).getText();
        return Integer.parseInt(likeText);
    }

    private static By likeSelector(int reviewId, String childSelector) {
        return reviewButtonSelector(reviewId, ".btn_like", childSelector);
    }

    private static By likeCountSelector(int reviewId) {
        return likeSelector(reviewId, " .btn-content");
    }

    private static By likeButtonSelector(int reviewId) {
        return likeSelector(reviewId, "");
    }

    public int dislikesCount(int reviewId) {
        String likeText = driver.findElement(dislikeCountSelector(reviewId)).getText();
        return Integer.parseInt(likeText);
    }

    private static By reviewButtonSelector(int reviewId, String buttonClassName, String childSelector) {
        String css = "%s[data-likeid='%d'] %s".formatted(buttonClassName, reviewId, childSelector).trim();
        return By.cssSelector(css);
    }

    private static By dislikeSelector(int reviewId, String childSelector) {
        return reviewButtonSelector(reviewId, ".btn_dislike", childSelector);
    }

    private static By dislikeCountSelector(int reviewId) {
        return dislikeSelector(reviewId, " .btn-content");
    }

    private static By dislikeButtonSelector(int reviewId) {
        return dislikeSelector(reviewId, "");
    }

    public BookPage clickOnLike(int reviewId) {
        driver.findElement(likeButtonSelector(reviewId)).click();
        return this;
    }

    public BookPage clickOnDislike(int reviewId) {
        driver.findElement(dislikeButtonSelector(reviewId)).click();
        return this;
    }

    public BookPage waitLikesCount(int reviewId, int count) {
        defaultWaiter()
                .until(ExpectedConditions.textToBe(likeCountSelector(reviewId), String.valueOf(count)));
        return this;
    }

    public BookPage waitDislikesCount(int reviewId, int count) {
        defaultWaiter()
                .until(ExpectedConditions.textToBe(dislikeCountSelector(reviewId), String.valueOf(count)));
        return this;
    }

    public int findReviewContaining(String phrase) {
        return findCommentByText(phrase)
                .map(c -> c.findElement(By.className("btn_like")).getAttribute("data-likeid"))
                .map(Integer::parseInt)
                .orElse(0)
        ;
    }

    private Optional<WebElement> findCommentByText(String phrase) {
        return driver.findElements(By.className("Comment")).stream()
                .filter(c -> c.findElement(By.className("Spoiler-visible")).getText().contains(phrase))
                .findAny();
    }

    public BookPage checkLikeIsHighlighted(int reviewId) {
        String css = driver.findElement(likeButtonSelector(reviewId)).getAttribute("class");
        assertThat(css).as("like button classes").contains("btn_check");
        return this;
    }

    public BookPage checkDislikeIsHighlighted(int reviewId) {
        String css = driver.findElement(dislikeButtonSelector(reviewId)).getAttribute("class");
        assertThat(css).as("dislike button classes").contains("btn_check");
        return this;
    }
}
