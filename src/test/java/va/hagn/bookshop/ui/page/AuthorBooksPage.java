package va.hagn.bookshop.ui.page;

import org.openqa.selenium.WebDriver;

public class AuthorBooksPage extends BooksPoolPage {
    public AuthorBooksPage(WebDriver driver) {
        super(driver);
    }

    public AuthorBooksPage open(String baseUrl, String slug) {
        super.open(baseUrl + "/books/author/" + slug);
        return this;
    }
}
