package va.hagn.bookshop.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchFragment extends Fragment {
    protected final By searchField = By.id("query");
    protected final By searchButton = By.id("search");

    public SearchFragment(WebDriver driver) {
        super(driver);
    }

    public void waitForSearchField() {
        defaultWaiter()
                .until(ExpectedConditions.visibilityOfElementLocated(searchField));
    }

    public SearchFragment typeQuery(String query) {
        clickOn(searchField);
        driver.findElement(searchField).sendKeys(query);
        return this;
    }

    public SearchPage clickOnSearch() {
        clickOn(searchButton);
        return new SearchPage(driver);
    }
}
