package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.MainPage;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class SmokeIT {
    private WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void changeLocale() {
        var mainPage = new MainPage(driver).open(baseUrl);

        assertEquals("Genres", mainPage.header().getGenresTabLabel());

        mainPage.footer().switchToRussianLocale();
        mainPage.waitUntilLoaded();

        assertEquals("Жанры", mainPage.header().getGenresTabLabel());
    }

    @Test void search() {
        new MainPage(driver).open(baseUrl).header().searchStrip()
                .typeQuery("eat")
                .clickOnSearch()
                .cards().waitUntilLoaded();
    }
}
