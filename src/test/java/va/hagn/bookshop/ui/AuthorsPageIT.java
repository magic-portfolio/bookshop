package va.hagn.bookshop.ui;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import va.hagn.bookshop.ui.driver.WebDriverExtension;
import va.hagn.bookshop.ui.page.AuthorPage;
import va.hagn.bookshop.ui.page.AuthorsPage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(WebDriverExtension.class)
public class AuthorsPageIT {
    private static WebDriver driver;
    @Value("${bookshop.baseurl}")
    private String baseUrl;

    @Test void authorWithoutBooks() {
        var parentPage = new AuthorsPage(driver).open(baseUrl);
        AuthorPage page = parentPage.clickOnFirstAuthor();
        page.waitUntilLoaded();
    }

    @Test void authorWithBooks() {
        var parentPage = new AuthorsPage(driver).open(baseUrl);
        AuthorPage authorPage = parentPage.clickOnAuthorWithMaxBooks()
                .waitUntilLoaded()
                .waitForStripToLoad();
        authorPage.showAllBooks()
                .waitUntilLoaded();
    }
}
