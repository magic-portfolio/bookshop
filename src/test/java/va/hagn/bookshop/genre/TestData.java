package va.hagn.bookshop.genre;

import va.hagn.bookshop.data.Genre;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

class TestData {
    static Map<Integer, Integer> sampleBookCounts() {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 10);
        map.put(11, 7);
        map.put(2, 5);
        map.put(3, 13);
        return map;
    }

    static Genre sampleGenres() {
        Genre root = aGenre(0, "Sentinel", "genre-aaa-000");
        root.setParent(root);
        Genre g1 = aGenre(1, "Easy reading", "genre-bac-111");
        Genre g11 = aGenre(11, "Sci Fi", "genre-ddb-339");
        addChild(g1, g11);
        Genre g2 = aGenre(2, "Non-fiction", "genre-xyz-313");
        Genre g3 = aGenre(3, "Horror", "genre-uvw-999");
        addChild(root, g1);
        addChild(root, g2);
        addChild(root, g3);

        return root;
    }
    
    static Genre aGenre(int id, String name, String slug) {
        Genre g = new Genre();
        g.setId(id);
        g.setName(name);
        g.setSlug(slug);
        return g;
    }

    static void  addChild(Genre parent, Genre child) {
        if (parent.getChildren() == null)
            parent.setChildren(new HashSet<>());
        parent.getChildren().add(child);
        child.setParent(parent);
    }

    static GenreDto sampleGenreDtos() {
        GenreDto root = new GenreDto("Sentinel", "genre-aaa-000", 777);
        GenreDto g1 = new GenreDto("Easy reading", "genre-bac-111", 10);
        GenreDto g11 = new GenreDto("Sci Fi", "genre-ddb-339", 7);
        g1.addChild(g11);
        GenreDto g2 = new GenreDto("Non-fiction", "genre-xyz-313", 5);
        GenreDto g3 = new GenreDto("Horror", "genre-uvw-999", 13);
        root.addChild(g1);
        root.addChild(g2);
        root.addChild(g3);
        return root;
    }
}
