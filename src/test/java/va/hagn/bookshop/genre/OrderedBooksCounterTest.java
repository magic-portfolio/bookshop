package va.hagn.bookshop.genre;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static va.hagn.bookshop.genre.TestData.sampleBookCounts;
import static va.hagn.bookshop.genre.TestData.sampleGenres;

class OrderedBooksCounterTest {
    @Test
    public void ordersByCount() {
        OrderedBooksCounter counter = new OrderedBooksCounter();
        GenreDto genreDto = counter.computeGenreTree(sampleGenres(), sampleBookCounts());
        assertNotNull(genreDto);
        assertTrue(genreDto.hasChildren());
    }
}
