package va.hagn.bookshop.genre;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class GenreServiceTest {
    @Test
    public void genreAmounts() {
        GenreService service = stubbedService();

        List<GenreDto> genreAmounts = service.getGenresWithBookAmounts();
        assertNotNull(genreAmounts);
        assertFalse(genreAmounts.isEmpty());
    }

    private GenreService stubbedService() {
        GenreRepository repository = Mockito.mock(GenreRepository.class);
        Mockito.when(repository.getGenreTree()).thenReturn(TestData.sampleGenres());
        Mockito.when(repository.getBooksCountPerGenreMap()).thenReturn(TestData.sampleBookCounts());

        BooksCounter booksCounter = Mockito.mock(BooksCounter.class);
        Mockito.when(booksCounter.computeGenreTree(Mockito.any(), Mockito.any())).thenReturn(TestData.sampleGenreDtos());

        return new GenreService(repository, booksCounter);
    }

}
