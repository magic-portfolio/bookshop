package va.hagn.bookshop.converters;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class StringToLocalDateConverterTest {

    @Test
    void convert() {
        StringToLocalDateConverter converter = new StringToLocalDateConverter();
        LocalDate dec = converter.convert("17.12.2021");
        assertNotNull(dec);
        assertEquals(17, dec.getDayOfMonth());
        assertEquals(2021, dec.getYear());
        assertEquals(Month.DECEMBER, dec.getMonth());
    }
}