package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RatingVotesTest {
    @Test
    public void averageRatingEqualVotes() {
        RatingVotes rv = new RatingVotes(7, 7, 7, 7, 7);
        assertEquals(3, rv.getAverageRating());
    }

    @Test
    public void averageRating() {
        RatingVotes rv = new RatingVotes(2, 3, 1, 5, 4);
        assertEquals(3, rv.getAverageRating());
    }

    @Test
    public void averageRatingOfEmptyList() {
        RatingVotes rv = new RatingVotes();
        assertEquals(0, rv.getAverageRating());
    }

    @Test
    public void averageRatingAllZeros() {
        RatingVotes rv = new RatingVotes(0, 0, 0, 0, 0);
        assertEquals(0, rv.getAverageRating());
    }

    @Test
    public void totalVotes() {
        RatingVotes rv = new RatingVotes(2, 3, 1, 5, 4);
        assertEquals(15, rv.getTotalVotes());
    }

    @Test
    public void totalVotesOfEmptyList() {
        RatingVotes rv = new RatingVotes();
        assertEquals(0, rv.getTotalVotes());
    }
}