package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TagServiceTest {
    @Test
    public void getStats() {
        TagService service = stubbedService(sampleTags());
        List<TagCategory> tagsCategories = service.getTagsCategories();
        assertNotNull(tagsCategories);
    }

    @Test
    public void computeCategories() {
        List<BooksCountPerTag> tags = sampleTags();
        List<TagCategory> tagsCategories = TagService.computeTagsCategories(tags, 5);
        assertNotNull(tagsCategories);
        assertIsOrdered(tagsCategories);
        checkCategoriesRange(tagsCategories);
    }

    private void checkCategoriesRange(List<TagCategory> tags) {
        assertTrue(tags.stream().map(TagCategory::getCategory).allMatch(c -> c >= 0 && c <= 4),
                "Some categories are outside range" + tags);
    }

    private TagService stubbedService(List<BooksCountPerTag> tags) {
        TagRepository repo = Mockito.mock(TagRepository.class);
        Mockito.when(repo.getTagsBooksCount()).thenReturn(tags);
        return new TagService(repo);
    }

    private List<BooksCountPerTag> sampleTags() {
        ThreadLocalRandom rndGen = ThreadLocalRandom.current();
        String[] names = {"lorem", "ipsum", "dolor", "dolce", "gusto"};
        List<BooksCountPerTag> tags = new ArrayList<>();
        for (int idx = 1; idx <= 50; idx++) {
            tags.add(new BooksCountPerTagStub(idx,
                    names[idx % names.length] + idx,
                    rndGen.nextInt(1, 1000)));
        }
        tags.sort(Comparator.comparingInt(BooksCountPerTag::getBooksCount).reversed());
        return tags;
    }

    private void assertIsOrdered(List<TagCategory> tagsCategories) {
        assertTrue(isSorted(tagsCategories, Comparator.comparing(TagCategory::getCategory)),
                "Not sorted by category " + tagsCategories);
    }

    public static <E> boolean isSorted(Iterable<E> collection, Comparator<E> comparator) {
        Iterator<E> iter = collection.iterator();

        if (!iter.hasNext()) return true;

        E previous = iter.next();
        while (iter.hasNext()) {
            E current = iter.next();
            if (comparator.compare(previous, current) > 0) {
                return false;
            }
            previous = current;
        }
        return true;
    }
}
