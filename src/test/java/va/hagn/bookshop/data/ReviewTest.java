package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class ReviewTest {

    @Test void creatingReviewFromEntity() {
        Review r = Review.from(TestData.review1(), "");
        assertReviewIsNotBlank(r);
        assertThat(r.getLikes()).isEqualTo(200);
        assertThat(r.getDislikes()).isEqualTo(100);
    }

    static void assertReviewIsNotBlank(Review r) {
        assertAll(
                () -> assertThat(r.getComment()).as("Comment").isNotBlank(),
                () -> assertThat(r.getRating()).as("Rating").isGreaterThan(0),
                () -> assertThat(r.getDate()).as("Date").isNotNull(),
                () -> assertThat(r.getDislikes()).as("Dislikes").isGreaterThan(0),
                () -> assertThat(r.getLikes()).as("Likes").isGreaterThan(0),
                () -> assertThat(r.getUserName()).as("User name").isNotBlank()
        );
    }
}