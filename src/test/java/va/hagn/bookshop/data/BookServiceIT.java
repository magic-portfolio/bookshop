package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class BookServiceIT {

    @Autowired
    private BookRepository repository;

    @Test void fetchFromDB() {
        var service = new BookService(repository, null, null);
        List<Book> books = service.getBooksData();
        assertThat(books).hasSizeGreaterThan(20);

        for (Book book : books) {
            checkIsNotBlank(book);
        }
    }

    private void checkIsNotBlank(Book book) {
        assert book.getAuthor() != null;
        assertThat(book.getTitle()).isNotBlank();
        assertThat(book.getPriceOld()).isNotNull();
        assertThat(book.getPrice()).isNotNull();
    }
}
