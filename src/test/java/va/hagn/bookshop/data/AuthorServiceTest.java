package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import va.hagn.bookshop.author.AuthorRepository;
import va.hagn.bookshop.author.AuthorService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorServiceTest {

    @Mock AuthorRepository repository;

    @Test
    void getAuthorsGroupedByLetter() {
        Author one = new Author("Blbul", "Hazaran");
        Author two = new Author("Narek", "Khorenazi");
        Author three = new Author("Hazar", "Unhaxt");
        Author four = new Author("Meer", "Zoray");
        when(repository.findAll()).thenReturn(List.of(one, three, two, four));

        var service = new AuthorService(repository);

        assertEquals("[H, K, U, Z]", service.getAuthorsGroupedByLetter().keySet().toString());
    }
}