package va.hagn.bookshop.data;

import java.util.Objects;

final class BooksCountPerTagStub implements BooksCountPerTag {
    private final int id;
    private final String name;
    private final int booksCount;

    public BooksCountPerTagStub(int id, String name, int booksCount) {
        Objects.requireNonNull(name, "Tag name");
        if (id < 1)
            throw new IllegalArgumentException("Tag id must be positive, but was " + id);
        if (booksCount < 0)
            throw new IllegalArgumentException("booksCount must not be negative, but was " + booksCount);

        this.id = id;
        this.name = name;
        this.booksCount = booksCount;
    }

    @Override
    public Integer getBooksCount() {
        return booksCount;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}
