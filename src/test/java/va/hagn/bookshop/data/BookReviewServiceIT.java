package va.hagn.bookshop.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(BookReviewService.class)
class BookReviewServiceIT {
    @Autowired
    private BookReviewService service;
    private String phrase;
    private Review review;

    @BeforeEach void setUp() {
        phrase = addRandomReview();
        review = fetchReview(phrase);
    }

    @Test void likeReview() {
        service.addReviewLike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());

        assertThat(fetchReview(phrase).getLikes()).isEqualTo(review.getLikes() + 1);
    }

    @Test void secondLikeUndoesIt() {
        service.addReviewLike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());
        service.addReviewLike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());

        assertThat(fetchReview(phrase).getLikes()).isEqualTo(review.getLikes());
    }

    @Test void dislikeReview() {
        service.addReviewDislike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());

        assertThat(fetchReview(phrase).getDislikes()).isEqualTo(review.getDislikes() + 1);
    }

    @Test void secondDislikeUndoesIt() {
        service.addReviewDislike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());
        service.addReviewDislike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());

        assertThat(fetchReview(phrase).getDislikes()).isEqualTo(review.getDislikes());
    }

    @Test void orderOfReviews() {
        service.addReviewDislike(review.getId(), TestData.guest.getHash(), LocalDateTime.now());
        addRandomReview();
        List<Review> reviews = service.getReviewsBySlug(TestData.cakeSlug, "");

        assertThat(reviews).isSortedAccordingTo(Comparator.comparing(r -> (r.getDislikes() - r.getLikes())));
    }

    private String addRandomReview() {
        String phrase = TestData.randomThreeWords();
        service.addReview(service.reviewParameters()
                .bookSlug(TestData.cakeSlug)
                .publicationDate(LocalDateTime.now())
                .rating(3)
                .userHash(TestData.guest.getHash())
                .text(phrase)
        );
        return phrase;
    }

    private Review fetchReview(String phrase) {
        List<Review> reviews = service.getReviewsBySlug(TestData.cakeSlug, "");
        return reviews.stream().filter(r -> r.getComment().contains(phrase)).findAny().orElseThrow();
    }

}
