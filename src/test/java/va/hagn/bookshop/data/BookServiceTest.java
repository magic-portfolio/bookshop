package va.hagn.bookshop.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static va.hagn.bookshop.data.TestData.aBookWithRatings;
import static va.hagn.bookshop.data.TestData.aRating;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {
    @Mock BookRepository repository;
    private BookService service;

    @BeforeEach
    void stubBookRepo() {
        Mockito.lenient().when(repository.findAll()).thenReturn(TestData.sampleBooks());

        service = new BookService(repository, new BooksRatingAndPopularityService(), null);
    }

    @Test
    public void ratingVotesBySlug() {
        Mockito.when(repository.findBookBySlug("asdf")).thenReturn(aBookWithRatings());

        RatingVotes votes = service.getRatingVotesBySlug("asdf");

        assertEquals(Arrays.asList(3, 2, 1, 0, 15), votes.getVotes());
    }

    @Test
    public void increaseVote() {
        Book book = aBookWithRatings();

        BookRating br = service.increaseVote(book.getBookRatings(), 3, book);
        assertEquals(3, br.getRating());
        assertEquals(2, br.getVotes());
    }

    @Test
    public void increaseVoteMissing() {
        Book book = aBookWithRatings();

        BookRating br = service.increaseVote(book.getBookRatings(), 4, book);
        assertEquals(4, br.getRating());
        assertEquals(1, br.getVotes());
    }

    @Test
    public void decreaseVote() {
        List<BookRating> ratings = Arrays.asList(
                aRating(1, 3),
                aRating(2, 2),
                aRating(3, 1),
                aRating(5, 15)
        );

        Optional<BookRating> br = service.decreaseVote(ratings, 3);
        assertTrue(br.isPresent());
        assertEquals(3, br.get().getRating());
        assertEquals(0, br.get().getVotes());
    }

    @Test
    public void decreaseVoteNoRating() {
        List<BookRating> ratings = Arrays.asList(
                aRating(1, 3),
                aRating(2, 2),
                aRating(3, 1),
                aRating(5, 15)
        );

        Optional<BookRating> br = service.decreaseVote(ratings, 4);
        assertFalse(br.isPresent());
    }

    @Test
    public void decreaseVoteZeroRating() {
        List<BookRating> ratings = Arrays.asList(
                aRating(1, 3),
                aRating(2, 2),
                aRating(3, 0),
                aRating(5, 15)
        );

        Optional<BookRating> br = service.decreaseVote(ratings, 3);
        assertFalse(br.isPresent());
    }

    @Test
    public void booksByPopularity() {
        List<Book> books = service.getBooksOrderedByPopularity();

        assertNotNull(books);
        assertEquals("3,1,2,4", bookIds(books));
    }

    @Test
    public void pageOfPopularBooks() {
        List<Book> books = service.getPageOfPopularBooks(2, 2).getContent();

        assertNotNull(books);
        assertEquals("2,4", bookIds(books));
    }

    private static String bookIds(List<Book> books) {
        return books.stream().map(b -> b.getId().toString()).collect(Collectors.joining(","));
    }
}
