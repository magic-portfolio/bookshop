package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import va.hagn.bookshop.author.AuthorRepository;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class AuthorRepositoryIT {

    @Autowired
    private AuthorRepository repository;

    @Test void fetchFromDB() {
        var authors = repository.findAll();
        assertThat(authors).hasSizeGreaterThan(100);

        for (Author a : authors) {
            checkIsNotBlank(a);
        }
    }

    private void checkIsNotBlank(Author a) {
        assertThat(a.getId()).isNotNull();
        assertThat(a.getFirstName()).isNotNull();
        assertThat(a.getLastName()).isNotBlank();
    }
}
