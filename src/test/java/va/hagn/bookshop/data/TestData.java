package va.hagn.bookshop.data;

import org.apache.commons.lang3.RandomStringUtils;
import va.hagn.bookshop.data.book.review.BookReviewEntity;
import va.hagn.bookshop.data.book.review.BookReviewLikeEntity;
import va.hagn.bookshop.data.user.UserEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class TestData {

    public static final Book one = new Book("Et arcu");
    public static final Book two = new Book("Dolor tempus");
    public static final Book three = new Book("Aliquam gravida");
    public static final List<Book> threeBooks = List.of(one, two, three);

    public static final Author lordByron = new Author("Lord", "Byron");
    public static final Book hordes = new Book("Hordes", lordByron);
    public static final String cakeSlug = "book-giu-246";
    public static final String slaughterhouseSlug = "book-nfi-353";
    public static final String wakeSlug = "book-wiv-198";
    public static final String flodderSlug = "book-uce-653";
    public static final UserEntity guest = new UserEntity(1, "Guest", "guest");

    public static List<Book> sampleBooks() {
        return Arrays.asList(
                aBook(1, 3, 3, 3),
                aBook(2, 2, 2, 2),
                aBook(3, 4, 4, 4),
                aBook(4, 1, 1, 1)
        );
    }

    public static Book aBook(int id, int bought, int cart, int kept) {
        Book book = new Book();
        book.setId(id);
        book.setBought(bought);
        book.setCart(cart);
        book.setKept(kept);
        return book;
    }

    public static String randomThreeWords() {
        return RandomStringUtils.randomAlphabetic(5, 10)
                + " " + RandomStringUtils.randomAlphabetic(5, 10)
                + " " + RandomStringUtils.randomAlphabetic(5, 10);
    }

    public static Book aBookWithRatings() {
        Book book = new Book();
        book.setBookRatings(Arrays.asList(
                aRating(1, 3, book),
                aRating(2, 2, book),
                aRating(3, 1, book),
                aRating(5, 15, book)
        ));
        return book;
    }

    public static BookRating aRating(int rating, int votes, Book book) {
        BookRating br = new BookRating();
        br.setRating(rating);
        br.setVotes(votes);
        br.setBook(book);
        return br;
    }

    public static BookRating aRating(int rating, int votes) {
        return aRating(rating, votes, null);
    }

    public static Book aBookWithReviews() {
        Book book = new Book();
        book.addReview(review1());
        book.addReview(review2());

        return book;
    }

    public static BookReviewEntity review1() {
        var r = new BookReviewEntity();
        r.setUser(guest);
        r.setRating(2);
        r.setTime(LocalDateTime.parse("2020-04-13T15:35:07"));
        r.setText("""
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit doli. Aenean commodo ligula eget dolor.
                Aenean massa. Cumtipsu sociis natoque penatibus et magnis dis parturient montesti, nascetur
                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eutu, pretiumem. Nulla consequat
                massa
                quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim
                justotuio,
                rhoncus ut loret, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
                pretium.
                Integer tincidunt. Cras dapibus. Vivamus element semper nisi. Aenean vulputate eleifend tellus.
                Aenean leo ligula, porttitor eu, consequat vitae.""");

        reviewLikes(200, 100).forEach(r::addReviewLike);

        return r;
    }

    private static List<BookReviewLikeEntity> reviewLikes(int likes, int dislikes) {
        List<BookReviewLikeEntity> le = new ArrayList<>();
        for (int idx = 0; idx < likes; idx++) {
            le.add(aReviewLike());
        }
        for (int idx = 0; idx < dislikes; idx++) {
            le.add(aReviewDislike());
        }
        return le;
    }

    private static BookReviewLikeEntity aReviewLike() {
        BookReviewLikeEntity e = new BookReviewLikeEntity();
        e.setValue((short) 1);
        return e;
    }

    private static BookReviewLikeEntity aReviewDislike() {
        BookReviewLikeEntity e = new BookReviewLikeEntity();
        e.setValue((short) -1);
        return e;
    }

    public static BookReviewEntity review2() {
        var r = new BookReviewEntity();
        r.setUser(guest);
        r.setRating(4);
        r.setTime(LocalDateTime.parse("2020-04-17T16:40:00"));
        r.setText("""
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit doli. Aenean commodo ligula eget
                dolor. Aenean massa. Cumtipsu sociis natoque penatibus et magnis dis parturient
                montesti,
                nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eutu, pretiumem.
                Nulla
                consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                arcu. In enim justotuio, rhoncus ut loret, imperdiet a, venenatis vitae, justo. Nullam
                dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus element
                semper
                nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat
                vitae.
                Первая книга - сборник повестей и рассказов "Большой поток" - посвящена людям
                леспромхоза.
                Автор книг "Рассказы" (1954), "Неразменное счастье. Повести и рассказы" (1962). Наиболее
                известная его повесть "Девчата" (1961) переведена на пятнадцать языков. По написанному
                им же
                сценарию был поставлен одноименный фильм, до сих пор имеющий огромный успех.""");

        reviewLikes(500, 50).forEach(r::addReviewLike);

        return r;
    }
}
