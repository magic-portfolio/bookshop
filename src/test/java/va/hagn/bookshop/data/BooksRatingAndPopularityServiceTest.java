package va.hagn.bookshop.data;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BooksRatingAndPopularityServiceTest {
    @Test
    public void nullValuesYieldZeroPopularity() {
        Book book = new Book();
        int popularity = new BooksRatingAndPopularityService().computeBookPopularity(book);
        assertEquals(0, popularity);
    }

    @Test
    public void popularityByBoughtOnly() {
        Book book = new Book();
        book.setBought(13);
        int popularity = new BooksRatingAndPopularityService().computeBookPopularity(book);
        assertEquals(13, popularity);
    }

    @Test
    public void popularityByCartOnly() {
        Book book = new Book();
        book.setCart(10);
        int popularity = new BooksRatingAndPopularityService().computeBookPopularity(book);
        assertEquals(7, popularity);
    }

    @Test
    public void popularityByKeptOnly() {
        Book book = new Book();
        book.setKept(10);
        int popularity = new BooksRatingAndPopularityService().computeBookPopularity(book);
        assertEquals(4, popularity);
    }

    @Test
    public void popularityInGeneralCase() {
        Book book = new Book();
        book.setBought(13);
        book.setCart(10);
        book.setKept(10);
        int popularity = new BooksRatingAndPopularityService().computeBookPopularity(book);
        assertEquals(24, popularity);
    }

    @Test
    public void booksPopularityComputation() {
        List<Book> books = TestData.sampleBooks();
        Map<Integer, Integer> popularity = new BooksRatingAndPopularityService()
                .computeBooksPopularity(books);

        assertEquals(idSet(books), popularity.keySet());
        assertEquals(emptyList(), negativeRatings(popularity));
    }

    private List<Integer> negativeRatings(Map<Integer, Integer> popularity) {
        return popularity.values().stream()
                .filter(v -> v <= 0)
                .collect(Collectors.toList());
    }

    private Set<Integer> idSet(List<Book> books) {
        return books.stream().map(Book::getId).collect(Collectors.toSet());
    }
}
