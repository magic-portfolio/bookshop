package va.hagn.bookshop.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static va.hagn.bookshop.data.TestData.aBookWithReviews;

@ExtendWith(MockitoExtension.class)
class BookReviewServiceTest {
    @Mock private BookRepository repository;
    @Mock private UserRepository userRepository;
    @Mock private BookReviewRepository bookReviewRepository;
    private BookReviewService service;

    @BeforeEach
    void stubBookRepo() {
        Mockito.lenient().when(repository.findBookBySlug(Mockito.anyString())).thenReturn(aBookWithReviews());

        service = new BookReviewService(repository, userRepository, bookReviewRepository);
    }

    @Test void reviewsBySlug() {

        var reviews = service.getReviewsBySlug("asdf", "");

        assertEquals(2, reviews.size());
        assertAll(
                () -> assertEquals(2, reviews.size()),
                () -> ReviewTest.assertReviewIsNotBlank(reviews.get(0)),
                () -> ReviewTest.assertReviewIsNotBlank(reviews.get(1))
        );
    }
}
