$(function () {
    $("#locales").change(function (event) {
        const lang = event.target.value;
        if (lang != null && ! /^\s*$/.test(lang)) {
            const searchParams = new URLSearchParams(window.location.search);
            searchParams.set("lang", lang);
            window.location.search = searchParams.toString();
        }
    });
});
