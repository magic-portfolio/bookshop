package va.hagn.bookshop.data;

import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import va.hagn.bookshop.errs.BookstoreApiWrongParameterException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final BooksRatingAndPopularityService ratingService;
    private final BookRatingRepository bookRatingRepository;

    public BookService(BookRepository bookRepository, BooksRatingAndPopularityService ratingService, BookRatingRepository bookRatingRepository) {
        this.bookRepository = bookRepository;
        this.ratingService = ratingService;
        this.bookRatingRepository = bookRatingRepository;
    }

    public List<Book> getBooksData() {
        return bookRepository.findAll();
    }

    //NEW BOOK SEVICE METHODS

    public List<Book> getBooksByAuthor(String authorName){
        return bookRepository.findBooksByAuthorFirstNameContaining(authorName);
    }

    public List<Book> getBooksByTitle(String title) throws BookstoreApiWrongParameterException {
        if (title == null || title.length() <= 1) {
            throw new BookstoreApiWrongParameterException("Wrong values passed to one or more parameters");
        } else {
            List<Book> data = bookRepository.findBooksByTitleContaining(title);
            if (data.size() > 0) {
                return data;
            } else {
                throw new BookstoreApiWrongParameterException("No data found with specified parameters...");
            }
        }
    }

    public List<Book> getBooksWithPriceBetween(Integer min, Integer max){
        return bookRepository.findBooksByPriceOldBetween(min,max);
    }

    public List<Book> getBooksWithPrice(Integer price){
        return bookRepository.findBooksByPriceOldIs(price);
    }

    public List<Book> getBooksWithMaxPrice(){
        return bookRepository.getBooksWithMaxDiscount();
    }

    public List<Book> getBestsellers(){
        return bookRepository.getBestsellers();
    }

    public Page<Book> getPageOfRecommendedBooks(Integer offset, Integer limit){
        Pageable nextPage = PageRequest.of(offset,limit);
        return bookRepository.findAll(nextPage);
    }

    public Page<Book> getPageOfRecentBooks(Integer offset, Integer limit){
        Pageable nextPage = PageRequest.of(offset,limit);
        return bookRepository.findAll(nextPage);
    }

    public Page<Book> getPageOfRecentBooksOrderedByDate(Integer offset, Integer limit, LocalDate fromDate, LocalDate toDate) {
        int pageNumber = offset / limit;
        Pageable nextPage = PageRequest.of(pageNumber, limit, Sort.by(Sort.Direction.DESC, "pubDate"));

        return bookRepository.findBooksByPubDateBetween(
                Date.valueOf(fromDate),
                Date.valueOf(toDate),
                nextPage);
    }

    public Page<Book> getPageOfPopularBooks(Integer offset, Integer limit){
        int pageNumber = offset / limit;
        Pageable nextPage = PageRequest.of(pageNumber,limit);
        return pageInList(getBooksOrderedByPopularity(), nextPage);
    }

    private static <E> Page<E> pageInList(List<E> list, Pageable pageable) {
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), list.size());

        return new PageImpl<>(list.subList(start, end), pageable, list.size());
    }

    public Page<Book> getPageOfSearchResultBooks(String searchWord, Integer offset, Integer limit){
        Pageable nextPage = PageRequest.of(offset,limit);
        return bookRepository.findBooksByTitleContaining(searchWord,nextPage);
    }

    public Book getBookBySlug(String slug) {
        return bookRepository.findBookBySlug(slug);
    }

    public void save(Book bookToUpdate) {
        bookRepository.save(bookToUpdate);
    }

    public RatingVotes getRatingVotesBySlug(String slug) {
        Book book = bookRepository.findBookBySlug(slug);

        List<BookRating> ratings = book.getBookRatings();
        Integer[] votes = {0, 0, 0, 0, 0};
        for (BookRating r : ratings) {
            votes[r.getRating() - 1] = r.getVotes();
        }

        return new RatingVotes(votes);
    }

    public List<Book> getBooksOrderedByPopularity() {
        List<Book> books = bookRepository.findAll();
        Map<Integer, Integer> popularity = ratingService.computeBooksPopularity(books);
        books.sort(Comparator.comparing((Book b) -> popularity.get(b.getId())).reversed());
        return books;
    }

    public void changeBookRating(String slug, Integer newRating, int oldRating) {
        if (slug == null || newRating == null) return;

        Book book = getBookBySlug(slug);
        List<BookRating> ratings = book.getBookRatings();

        Optional<BookRating> oldBR = decreaseVote(ratings, oldRating);
        oldBR.ifPresent(bookRatingRepository::save);

        BookRating newBR = increaseVote(ratings, newRating, book);
        bookRatingRepository.save(newBR);
    }

    Optional<BookRating> decreaseVote(List<BookRating> ratings, int oldRating) {
        if (ratings == null || oldRating < 1)
            return Optional.empty();

        Optional<BookRating> rating = ratings.stream()
                .filter(br -> br.getRating() == oldRating)
                .filter(br -> br.getVotes() > 0)
                .findAny();
        rating.ifPresent(br -> br.setVotes(br.getVotes() - 1));

        return rating;
    }

    BookRating increaseVote(List<BookRating> ratings, int newRating, Book book) {
        Optional<BookRating> maybeRating = ratings.stream()
                .filter(br -> br.getRating() == newRating)
                .findAny();

        BookRating rating;
        if (maybeRating.isPresent()) {
            rating = maybeRating.get();
            rating.setVotes(rating.getVotes() + 1);
        } else {
            rating = new BookRating();
            rating.setBook(book);
            rating.setRating(newRating);
            rating.setVotes(1);
        }
        return rating;
    }
}
