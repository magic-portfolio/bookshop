package va.hagn.bookshop.data.book.file;

import javax.persistence.*;

//@Entity
//@Table(name = "book_file")
public class BookFileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String hash;

    @ManyToOne(optional = false)
    @JoinColumn(name="type_id")
    private BookFileTypeEntity fileType;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String path;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public BookFileTypeEntity getFileType() {
        return fileType;
    }

    public void setFileType(BookFileTypeEntity fileType) {
        this.fileType = fileType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
