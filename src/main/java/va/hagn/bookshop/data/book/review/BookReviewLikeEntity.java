package va.hagn.bookshop.data.book.review;

import com.fasterxml.jackson.annotation.JsonIgnore;
import va.hagn.bookshop.data.user.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "book_review_like")
public class BookReviewLikeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "review_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private BookReviewEntity bookReview;

    @ManyToOne(optional = false)
    @JsonIgnore
    private UserEntity user;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime time;

    @Column(columnDefinition = "SMALLINT", nullable = false)
    private short value;

    public BookReviewLikeEntity() {

    }

    public BookReviewLikeEntity(UserEntity user, short value, LocalDateTime time) {
        this.user = user;
        this.value = value;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookReviewEntity getBookReview() {
        return bookReview;
    }

    public void setBookReview(BookReviewEntity bookReview) {
        this.bookReview = bookReview;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public boolean wasMadeByUser(String hash) {
        return this.user != null && hash.equals(this.user.getHash());
    }
}
