package va.hagn.bookshop.data.book.review;

import com.fasterxml.jackson.annotation.JsonIgnore;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.user.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "book_review")
public class BookReviewEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Book book;

    @ManyToOne(optional = false)
    @JsonIgnore
    private UserEntity user;

    @Column(nullable = false)
    private int rating;

    @Column(nullable = false)
    private LocalDateTime time;

    @Column(nullable = false)
    private String text;

    @OneToMany(mappedBy = "bookReview", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<BookReviewLikeEntity> reviewLikes = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<BookReviewLikeEntity> getReviewLikes() {
        return Collections.unmodifiableList(reviewLikes);
    }

    public void addReviewLike(BookReviewLikeEntity like) {
        like.setBookReview(this);
        this.reviewLikes.add(like);
    }

    public void removeLikeById(int likeId) {
        reviewLikes.removeIf(rl -> rl.getId() == likeId);
    }

    public Optional<BookReviewLikeEntity> findReviewLike(String userHash, short value) {
        return reviewLikes.stream()
                .filter(rl -> rl.wasMadeByUser(userHash) && rl.getValue() == value)
                .findAny();
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
