package va.hagn.bookshop.data.user;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String hash;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime regTime;

    @Column(columnDefinition = "INT", nullable = false)
    private int balance;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String name;

    public UserEntity() {

    }

    public UserEntity(int id, String name, String hash) {
        this.id = id;
        this.name = name;
        this.hash = hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public LocalDateTime getRegTime() {
        return regTime;
    }

    public void setRegTime(LocalDateTime regTime) {
        this.regTime = regTime;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
