package va.hagn.bookshop.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    @Query("select t.id as id, t.name as name, count(b) as booksCount from Book b join b.tags t group by t order by count(b) desc")
    List<BooksCountPerTag> getTagsBooksCount();

    @Query("select b from Book b join b.tags t where t.id = :id")
    Page<Book> findBooksByTagId(Integer id, Pageable pageable);
}
