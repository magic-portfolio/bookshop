package va.hagn.bookshop.data;

import org.springframework.stereotype.Service;
import va.hagn.bookshop.data.book.review.BookReviewEntity;
import va.hagn.bookshop.data.book.review.BookReviewLikeEntity;
import va.hagn.bookshop.data.user.UserEntity;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class BookReviewService {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final BookReviewRepository bookReviewRepository;

    public BookReviewService(BookRepository bookRepository, UserRepository userRepository,
                             BookReviewRepository bookReviewRepository) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
        this.bookReviewRepository = bookReviewRepository;
    }

    public List<Review> getReviewsBySlug(String slug, String userHash) {
        Book book = bookRepository.findBookBySlug(slug);

        if (book == null)
            return List.of();

        return book.getBookReviews().stream()
                .map(br -> Review.from(br, userHash))
                .sorted(Comparator.comparing((Review r) -> (r.getDislikes() - r.getLikes()))
                        .thenComparing(Review::getDate))
                .toList();
    }

    public void addReview(ReviewParameters params) {
        bookRepository.save(params.bookEntity());
    }

    public ReviewParameters reviewParameters() {
        return new ReviewParameters();
    }

    public void addReviewLike(Integer reviewid, String userHash, LocalDateTime time) {
        addReviewLikeEntity(reviewid, userHash, (short) 1, time);
    }

    public void addReviewDislike(Integer reviewid, String userHash, LocalDateTime time) {
        addReviewLikeEntity(reviewid, userHash, (short) -1, time);
    }

    private void addReviewLikeEntity(Integer reviewid, String userHash, short value, LocalDateTime time) {
        var user = userRepository.findByHash(userHash).orElseThrow(
                () -> new NoSuchElementException("No user found with hash " + userHash));
        var br = bookReviewRepository.findById(reviewid).orElseThrow(() ->
                new NoSuchElementException("No review found with id " + reviewid));

        Optional<BookReviewLikeEntity> sameLike = br.findReviewLike(userHash, value);
        if (sameLike.isPresent()) {
            br.removeLikeById(sameLike.get().getId());
        } else {
            Optional<BookReviewLikeEntity> oppositeLike = br.findReviewLike(userHash, (short) -value);
            oppositeLike.ifPresent(ol -> br.removeLikeById(ol.getId()));

            BookReviewLikeEntity like = new BookReviewLikeEntity(user, value, time);
            br.addReviewLike(like);
        }

        bookReviewRepository.save(br);
    }

    public class ReviewParameters {
        private String bookSlug;
        private String userHash;
        private String text;
        private LocalDateTime publicationDate;
        private int rating;

        // populated at the build stage
        private Book book;
        private UserEntity user;

        private ReviewParameters() {
        }

        public ReviewParameters bookSlug(String bookSlug) {
            this.bookSlug = bookSlug;
            return this;
        }

        public ReviewParameters userHash(String userHash) {
            this.userHash = userHash;
            return this;
        }

        public ReviewParameters text(String text) {
            this.text = text;
            return this;
        }

        public ReviewParameters publicationDate(LocalDateTime publicationDate) {
            this.publicationDate = publicationDate;
            return this;
        }

        public ReviewParameters rating(int rating) {
            this.rating = rating;
            return this;
        }

        private Book bookEntity() {
            book = bookRepository.findBookBySlug(bookSlug);
            user = userRepository.findByHash(userHash).get();

            book.addReview(reviewEntity());

            return book;
        }

        private BookReviewEntity reviewEntity() {
            BookReviewEntity br = new BookReviewEntity();
            br.setBook(book);
            br.setUser(user);
            br.setText(text);
            br.setTime(publicationDate);
            br.setRating(rating);

            return br;
        }
    }
}
