package va.hagn.bookshop.data;

import java.util.Objects;

public final class TagCategory {

    private final int id;
    private final String name;
    private final int category;

    public TagCategory(int id, String name, int category) {
        Objects.requireNonNull(name, "Tag name");
        if (id < 1)
            throw new IllegalArgumentException("Tag id must be positive, but was " + id);
        if (category < 0)
            throw new IllegalArgumentException("category must not be negative, but was " + category);

        this.id = id;
        this.name = name;
        this.category = category;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "TagCategory{" +
                "name='" + name + '\'' +
                ", category=" + category +
                '}';
    }
}
