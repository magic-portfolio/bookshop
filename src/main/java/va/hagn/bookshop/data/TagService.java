package va.hagn.bookshop.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TagService {
    private final TagRepository repository;

    public TagService(TagRepository repository) {
        this.repository = repository;
    }

    public String getTagNameById(Integer id) {
        return repository.findById(id).get().getName();
    }

    public List<TagCategory> getTagsCategories() {
        List<BooksCountPerTag> tagsBooksCount = repository.getTagsBooksCount();
        List<TagCategory> tags = computeTagsCategories(tagsBooksCount, 5);
        Collections.shuffle(tags);
        return tags;
    }

    static List<TagCategory> computeTagsCategories(List<BooksCountPerTag> tags, int numberOfCategories) {
        List<TagCategory> list = new ArrayList<>();
        int idx = 0, category = 1, size = tags.size();

        for (BooksCountPerTag t : tags) {
            if (numberOfCategories * ++idx / category > size) category++;

            list.add(new TagCategory(t.getId(), t.getName(), category - 1));
        }
        return list;
    }

    public Page<Book> getPageOfBooksByTagId(Integer id, int offset, int limit) {
        int pageNumber = offset / limit;
        Pageable nextPage = PageRequest.of(pageNumber, limit);

        return repository.findBooksByTagId(id, nextPage);
    }
}
