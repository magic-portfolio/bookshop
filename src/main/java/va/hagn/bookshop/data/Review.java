package va.hagn.bookshop.data;

import va.hagn.bookshop.data.book.review.BookReviewEntity;
import va.hagn.bookshop.data.book.review.BookReviewLikeEntity;

import java.time.format.DateTimeFormatter;

public class Review {
    private String userName;
    private String date;
    private String comment;
    private String spoiler;
    private int likes;
    private int dislikes;
    private int rating;
    private int id;
    private boolean likedByCurrentUser;
    private boolean dislikedByCurrentUser;

    public Review() {
    }

    public static Review from(BookReviewEntity br, String userHash) {
        Review r = new Review();
        r.setId(br.getId());
        r.setComment(br.getText());
        r.setRating(br.getRating());
        r.setDate(br.getTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")));
        r.setUserName(br.getUser().getName());

        int likes = 0, dislikes = 0;
        for (BookReviewLikeEntity like : br.getReviewLikes()) {
            short value = like.getValue();

            if (value < 0)
                dislikes++;
            else
                likes++;
        }
        r.setLikes(likes);
        r.setDislikes(dislikes);

        r.setLikedByCurrentUser(br.findReviewLike(userHash, (short) 1).isPresent());
        r.setDislikedByCurrentUser(br.findReviewLike(userHash, (short) -1).isPresent());

        return r;
    }

    @Override
    public String toString() {
        return "Review{id=%d, +%d, -%d}".formatted(id, likes, dislikes);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSpoiler() {
        return spoiler;
    }

    public boolean hasSpoiler() {
        return !(spoiler == null || spoiler.isBlank());
    }

    public void setSpoiler(String spoiler) {
        this.spoiler = spoiler;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setLikedByCurrentUser(boolean likedByCurrentUser) {
        this.likedByCurrentUser = likedByCurrentUser;
    }

    public boolean isLikedByCurrentUser() {
        return likedByCurrentUser;
    }

    public void setDislikedByCurrentUser(boolean dislikedByCurrentUser) {
        this.dislikedByCurrentUser = dislikedByCurrentUser;
    }

    public boolean isDislikedByCurrentUser() {
        return dislikedByCurrentUser;
    }
}
