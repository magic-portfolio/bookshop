package va.hagn.bookshop.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class RatingVotes {
    private final List<Integer> votes;

    public RatingVotes(Integer... votes) {
        this.votes = Arrays.asList(votes);
    }

    public int getAverageRating() {
        if (votes == null || getTotalVotes() == 0) return 0;

        int sum = IntStream.rangeClosed(1, votes.size()).map(r -> r * votes.get(r - 1)).sum();

        return sum / getTotalVotes();
    }

    public int getTotalVotes() {
        return votes.stream().reduce(0, Integer::sum);
    }

    public List<Integer> getVotes() {
        return Collections.unmodifiableList(votes);
    }
}
