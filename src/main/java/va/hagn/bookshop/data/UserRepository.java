package va.hagn.bookshop.data;

import org.springframework.data.repository.CrudRepository;
import va.hagn.bookshop.data.user.UserEntity;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByHash(String hash);
}
