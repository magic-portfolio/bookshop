package va.hagn.bookshop.data;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * This thing could be easily implemented as a repository method with an SQL query.
 * However, I'm keeping this in Java as an exercise of having a complex calculation.
 */
@Service
public class BooksRatingAndPopularityService {
    /**
     * It is assumed that bought, kept, and cart values are within 1 million, so no arithmetic overflow is expected.
     * Otherwise, switch to long or even BigInteger.
     */
    public int computeBookPopularity(Book book) {
        Objects.requireNonNull(book, "Cannot handle null books");

        return (10 * book.getBought() + book.getCart() * 7 + book.getKept() * 4) / 10;
    }

    /**
     * The algorithm assumes that each book has a non-null unique id.
     */
    public Map<Integer, Integer> computeBooksPopularity(Iterable<Book> books) {
        Map<Integer, Integer> popularity = new HashMap<>();
        for (Book book : books) {
            popularity.put(book.getId(), computeBookPopularity(book));
        }
        return popularity;
    }
}
