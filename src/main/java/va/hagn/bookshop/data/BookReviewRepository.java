package va.hagn.bookshop.data;

import org.springframework.data.repository.CrudRepository;
import va.hagn.bookshop.data.book.review.BookReviewEntity;

public interface BookReviewRepository extends CrudRepository<BookReviewEntity, Integer> {
}
