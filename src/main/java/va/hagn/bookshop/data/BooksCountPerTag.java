package va.hagn.bookshop.data;

public interface BooksCountPerTag {
    Integer getId();
    String getName();
    Integer getBooksCount();
}
