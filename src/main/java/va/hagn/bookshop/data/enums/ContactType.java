package va.hagn.bookshop.data.enums;

public enum ContactType {
    PHONE,
    EMAIL;
}
