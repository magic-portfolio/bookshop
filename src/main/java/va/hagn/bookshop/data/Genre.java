package va.hagn.bookshop.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "genres")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true)
    private String slug;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "parent_id")
    @JsonIgnore
    private Genre parent;

    @OneToMany(mappedBy = "parent")
    @JsonIgnore
    private Collection<Genre> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getParent() {
        return parent;
    }

    public void setParent(Genre parent) {
        this.parent = parent;
    }

    public Collection<Genre> getChildren() {
        return children;
    }

    public void setChildren(Collection<Genre> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", slug='" + slug + '\'' +
                ", name='" + name + '\'' +
                ", parent_id=" + (parent == null ? "none" : parent.id) +
                '}';
    }
}
