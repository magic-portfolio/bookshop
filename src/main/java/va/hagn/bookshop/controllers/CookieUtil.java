package va.hagn.bookshop.controllers;

import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CookieUtil {

    public Cookie addSlug(String slug, Cookie cookie) {
        cookie.setValue(addSlug(slug, cookie.getValue()));
        cookie.setPath("/");
        return cookie;
    }

    public String addSlug(String slug, String content) {
        if (content == null || content.isEmpty()) {
            return slug;
        } else if (content.contains(slug)) {
            return content;
        } else {
            StringJoiner stringJoiner = new StringJoiner("/");
            stringJoiner.add(content).add(slug);
            return stringJoiner.toString();
        }
    }

    public Cookie removeSlug(String slug, Cookie cookie) {
        cookie.setValue(removeSlug(slug, cookie.getValue()));
        cookie.setPath("/");
        return cookie;
    }

    public String removeSlug(String slug, String content) {
        if (content == null || content.isEmpty()) {
            return content;
        } else {
            List<String> cookieBooks = new ArrayList<>(Arrays.asList(content.split("/")));
            cookieBooks.remove(slug);
            return String.join("/", cookieBooks);
        }
    }

    public List<String> slugsInCookie(String content) {
        if (content == null) {
            return Collections.emptyList();
        } else {
            return Arrays.stream(content.split("/"))
                    .filter(e -> !e.isEmpty())
                    .collect(Collectors.toList());
        }
    }

    public Cookie changeSlugRating(String slug, Integer rating, Cookie cookie) {
        cookie.setValue(changeSlugRating(slug, rating, cookie.getValue()));
        cookie.setPath("/");
        return cookie;
    }

    public String changeSlugRating(String slug, Integer rating, String content) {
        if (content == null || content.isEmpty())
            return slug + ":" + rating;

        List<String> cookieBooks = new ArrayList<>(Arrays.asList(content.split("/")));
        cookieBooks.removeIf(b -> b.contains(slug));
        cookieBooks.add(slug + ":" + rating);

        return String.join("/", cookieBooks);
    }

    public int getSlugRating(String slug, String content) {
        if (content == null || !content.contains(slug))
            return 0;
        return Arrays.stream(content.split("/"))
                .filter(e -> e.startsWith(slug))
                .map(e -> e.split(":")[1])
                .map(Integer::parseInt)
                .findFirst().orElse(0);
    }
}
