package va.hagn.bookshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.BooksPageDto;
import va.hagn.bookshop.data.SearchWordDto;
import va.hagn.bookshop.data.TagService;

@Controller
public class TagPageController {
    private final TagService tagService;

    public TagPageController(TagService tagService) {
        this.tagService = tagService;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/tags/{id}")
    public String mainPage(@PathVariable Integer id, Model model) {
        model.addAttribute("tagId", id);
        model.addAttribute("tagName", tagService.getTagNameById(id));
        model.addAttribute("booksList", tagService.getPageOfBooksByTagId(id, 0, 20).getContent());

        return "tags/index";
    }

    @GetMapping("/books/tag/{id}")
    @ResponseBody
    public BooksPageDto getBooksByTag(@PathVariable Integer id,
                                      @RequestParam Integer offset,
                                      @RequestParam Integer limit) {
        return new BooksPageDto(tagService.getPageOfBooksByTagId(id, offset, limit).getContent());
    }
}
