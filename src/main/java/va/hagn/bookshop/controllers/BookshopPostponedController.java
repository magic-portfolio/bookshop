package va.hagn.bookshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.BookRepository;
import va.hagn.bookshop.data.SearchWordDto;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BookshopPostponedController {

    @Autowired CookieUtil cookieUtil;

    private final BookRepository bookRepository;

    @Autowired
    public BookshopPostponedController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/postponed")
    public String showPostponedBooks(@CookieValue(value = "keptContents", required = false) String keptContents,
                                     Model model) {
        List<String> slugs = cookieUtil.slugsInCookie(keptContents);
        model.addAttribute("noKeptBooks", slugs.isEmpty());

        List<Book> booksFromCookieSlugs = bookRepository.findBooksBySlugIn(slugs);
        model.addAttribute("books", booksFromCookieSlugs);

        return "postponed";
    }

    @PostMapping("/changeBookStatus/kept/remove/{slug}")
    public String removeBookFromPostponed(
            @PathVariable("slug") String slug,
            @CookieValue(name = "keptContents", required = false) String keptContents,
            HttpServletResponse response) {

        response.addCookie(cookieUtil.removeSlug(slug, new Cookie("keptContents", keptContents)));

        return "redirect:/books/postponed";
    }

    @PostMapping("/changeBookStatus/kept/{slug}")
    public String addBookToPostponed(
            @PathVariable("slug") String slug,
            @CookieValue(name = "cartContents", required = false) String cartContents,
            @CookieValue(name = "keptContents", required = false) String keptContents,
            HttpServletResponse response) {

        response.addCookie(cookieUtil.addSlug(slug, new Cookie("keptContents", keptContents)));
        response.addCookie(cookieUtil.removeSlug(slug, new Cookie("cartContents", cartContents)));

        return "redirect:/books/" + slug;
    }
}
