package va.hagn.bookshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.BookRepository;
import va.hagn.bookshop.data.SearchWordDto;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BookshopCartController {

    @Autowired CookieUtil cookieUtil;

    private final BookRepository bookRepository;

    @Autowired
    public BookshopCartController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/cart")
    public String handleCartRequest(@CookieValue(value = "cartContents", required = false) String cartContents,
                                    Model model) {
        List<String> slugs = cookieUtil.slugsInCookie(cartContents);
        model.addAttribute("isCartEmpty", slugs.isEmpty());

        List<Book> booksFromCookieSlugs = bookRepository.findBooksBySlugIn(slugs);
        model.addAttribute("bookCart", booksFromCookieSlugs);

        return "cart";
    }

    @PostMapping("/changeBookStatus/cart/remove/{slug}")
    public String handleRemoveBookFromCartRequest(
            @PathVariable("slug") String slug,
            @CookieValue(name = "cartContents", required = false) String cartContents,
            HttpServletResponse response) {

        response.addCookie(cookieUtil.removeSlug(slug, new Cookie("cartContents", cartContents)));

        return "redirect:/books/cart";
    }

    @PostMapping("/changeBookStatus/cart/{slug}")
    public String handleChangeBookStatus(
            @PathVariable("slug") String slug,
            @CookieValue(name = "cartContents", required = false) String cartContents,
            @CookieValue(name = "keptContents", required = false) String keptContents,
            HttpServletResponse response) {

        response.addCookie(cookieUtil.addSlug(slug, new Cookie("cartContents", cartContents)));
        response.addCookie(cookieUtil.removeSlug(slug, new Cookie("keptContents", keptContents)));

        return "redirect:/books/" + slug;
    }
}
