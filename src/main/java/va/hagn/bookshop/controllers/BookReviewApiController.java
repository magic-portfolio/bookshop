package va.hagn.bookshop.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.BookReviewService;

import java.time.LocalDateTime;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Tag(name = "book review api")
public class BookReviewApiController {
    private final CookieUtil cookieUtil;

    private final BookReviewService bookReviewService;

    public BookReviewApiController(BookReviewService bookReviewService, CookieUtil cookieUtil) {
        this.cookieUtil = cookieUtil;
        this.bookReviewService = bookReviewService;
    }

    @PostMapping("/bookReview")
    public Map<String, Boolean> bookReview(@RequestParam(value = "bookId", required = false) String slug,
                                           @RequestParam(required = false) String text,
                                           @CookieValue(required = false) String bookRatings) {
        if (slug == null || slug.isBlank() || text == null || text.isBlank())
            return Map.of("result", false);

        BookReviewService.ReviewParameters params = bookReviewService.reviewParameters()
                .bookSlug(slug)
                .userHash("guest") // todo we should get it from the session
                .text(text)
                .rating(cookieUtil.getSlugRating(slug, bookRatings))
                .publicationDate(LocalDateTime.now());

        bookReviewService.addReview(params);

        return Map.of("result", true);
    }

    @PostMapping("/rateBookReview")
    public Map<String, Object> rateReview(@RequestParam(required = false) Integer reviewid,
                                           @RequestParam(required = false) Integer value) {
        if (reviewid == null || value == null)
            return Map.of("result", false);

        String userHash = "guest"; // todo we should get it from the session
        var time = LocalDateTime.now();

        try {
            if (value > 0)
                bookReviewService.addReviewLike(reviewid, userHash, time);
            else
                bookReviewService.addReviewDislike(reviewid, userHash, time);
        } catch (Exception ex) {
            return Map.of("result", false, "error", ex.getMessage());
        }

        return Map.of("result", true);
    }
}
