package va.hagn.bookshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import va.hagn.bookshop.data.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Controller
@RequestMapping("/books")
public class BooksController {
    @Autowired CookieUtil cookieUtil;

    private final BookService bookService;
    private final BookReviewService bookReviewService;
    private final ResourceStorage storage;

    public BooksController(BookService bookService, BookReviewService bookReviewService, ResourceStorage storage) {
        this.bookService = bookService;
        this.bookReviewService = bookReviewService;
        this.storage = storage;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/{slug}")
    public String bookPage(@PathVariable("slug") String slug,
                           @CookieValue(name = "bookRatings", required = false) String bookRatings,
                           Model model) {
        RatingVotes rating = bookService.getRatingVotesBySlug(slug);

        model.addAttribute("slugBook", bookService.getBookBySlug(slug));
        model.addAttribute("averageRating", rating.getAverageRating());
        model.addAttribute("totalVotes", rating.getTotalVotes());
        model.addAttribute("votes", rating.getVotes());
        model.addAttribute("bookRating", cookieUtil.getSlugRating(slug, bookRatings));

        // todo we should get the hash from the session
        List<Review> reviews = bookReviewService.getReviewsBySlug(slug, "guest");
        model.addAttribute("reviews", reviews);
        model.addAttribute("totalReviews", reviews.size());

        return "/books/slug";
    }

    @PostMapping("/{slug}/img/save")
    public String saveNewBoookImage(@RequestParam("file") MultipartFile file, @PathVariable("slug") String slug) throws IOException {

        String savePath = storage.saveNewBookImage(file, slug);
        Book bookToUpdate = bookService.getBookBySlug(slug);
        bookToUpdate.setImage(savePath);
        bookService.save(bookToUpdate); //save new path in db here

        return ("redirect:/books/" + slug);
    }

    @GetMapping("/download/{hash}")
    public ResponseEntity<ByteArrayResource> bookFile(@PathVariable("hash") String hash) throws IOException {

        Path path = storage.getBookFilePath(hash);
        Logger.getLogger(this.getClass().getSimpleName()).info("book file path: " + path);

        MediaType mediaType = storage.getBookFileMime(hash);
        Logger.getLogger(this.getClass().getSimpleName()).info("book file mime type: " + mediaType);

        byte[] data = storage.getBookFileByteArray(hash);
        Logger.getLogger(this.getClass().getSimpleName()).info("book file data len: " + data.length);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
                .contentType(mediaType)
                .contentLength(data.length)
                .body(new ByteArrayResource(data));
    }

    @PostMapping("/changeBookStatus/rating")
    @ResponseBody
    public Map<String, Boolean> changeBookRating(
            @RequestParam("bookId") String slug,
            @RequestParam("value") Integer rating,
            @CookieValue(name = "bookRatings", required = false) String bookRatings,
            HttpServletResponse response) {

        bookService.changeBookRating(slug, rating, cookieUtil.getSlugRating(slug, bookRatings));
        response.addCookie(cookieUtil.changeSlugRating(slug, rating, new Cookie("bookRatings", bookRatings)));

        return Map.of("result", true);
    }
}
