package va.hagn.bookshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.SearchWordDto;

@Controller
public class PopularPageController {
    private final BookService bookService;

    public PopularPageController(BookService bookService) {
        this.bookService = bookService;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/books/popular")
    public String mainPage(Model model) {
        model.addAttribute("booksList",
                bookService.getPageOfPopularBooks(0, 20).getContent());
        return "books/popular";
    }
}
