package va.hagn.bookshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.BookService;
import va.hagn.bookshop.data.BooksPageDto;
import va.hagn.bookshop.data.SearchWordDto;

import java.time.LocalDate;
import java.util.List;

@Controller
public class RecentPageController {
    private final BookService bookService;

    public RecentPageController(BookService bookService) {
        this.bookService = bookService;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @ModelAttribute("booksList")
    public List<Book> booksList() {
        return bookService.getPageOfRecentBooksOrderedByDate(0, 20, LocalDate.now().minusMonths(1L), LocalDate.now()).getContent();
    }

    @GetMapping("/books/recent")
    public String recentBooksPage() {
        return "books/recent";
    }

    @GetMapping(value = "/books/recent", params = {"offset", "limit", "from", "to"})
    @ResponseBody
    public BooksPageDto getRecentBooksPage(@RequestParam Integer offset,
                                           @RequestParam Integer limit,
                                           @RequestParam LocalDate from,
                                           @RequestParam LocalDate to) {
        return new BooksPageDto(bookService.getPageOfRecentBooksOrderedByDate(offset, limit, from, to).getContent());
    }
}
