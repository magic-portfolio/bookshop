package va.hagn.bookshop.genre;

import org.springframework.boot.CommandLineRunner;
import va.hagn.bookshop.data.Genre;

import java.util.List;
import java.util.Map;

//@Component
public class GenreRepoToaster implements CommandLineRunner {
    private final GenreRepository repository;

    public GenreRepoToaster(GenreRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) {
        List<Genre> genres = repository.findAll();
        System.out.println("Genres: " + genres);

        Map<Integer, Integer> counts = repository.getBooksCountPerGenreMap();
        System.out.println("Book counts: " + counts);

        Genre groot = repository.getGenreTree();
        System.out.println("root children: " + groot.getChildren());

        for (Genre child : groot.getChildren()) {
            System.out.println(child.getName() + ": " + child.getChildren());
        }
    }
}
