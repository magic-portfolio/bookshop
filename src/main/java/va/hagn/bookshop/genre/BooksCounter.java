package va.hagn.bookshop.genre;

import va.hagn.bookshop.data.Genre;

import java.util.Map;

public interface BooksCounter {
    GenreDto computeGenreTree(Genre tree, Map<Integer, Integer> booksCountPerGenre);
}
