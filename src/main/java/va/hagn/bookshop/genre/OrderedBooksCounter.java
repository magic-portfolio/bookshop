package va.hagn.bookshop.genre;

import org.springframework.stereotype.Component;
import va.hagn.bookshop.data.Genre;

import java.util.Comparator;
import java.util.Map;
import java.util.stream.Stream;

@Component
public class OrderedBooksCounter implements BooksCounter {
    @Override
    public GenreDto computeGenreTree(Genre tree, Map<Integer, Integer> booksCountPerGenre) {
        GenreDto root = GenreDto.fromGenre(tree, booksCountPerGenre.getOrDefault(tree.getId(), 0));
        childrenOf(tree)
                .map(child -> computeGenreTree(child, booksCountPerGenre))
                .sorted(Comparator.comparingInt(GenreDto::getAmount).reversed())
                .forEach(root::addChild);
        return root;
    }

    private Stream<Genre> childrenOf(Genre tree) {
        return (tree == null || tree.getChildren() == null) ? Stream.empty() : tree.getChildren().stream();
    }
}
