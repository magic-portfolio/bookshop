package va.hagn.bookshop.genre;

import org.springframework.stereotype.Component;
import va.hagn.bookshop.data.Genre;

import java.util.Map;

@Component
public class CannedBooksCounter implements BooksCounter{
    @Override
    public GenreDto computeGenreTree(Genre tree, Map<Integer, Integer> booksCountPerGenre) {
        GenreDto root = new GenreDto("Sentinel", "genre-aaa-000", 777);
        GenreDto g1 = new GenreDto("Easy reading", "genre-bac-111", 10);
        GenreDto g11 = new GenreDto("Sci Fi", "genre-ddb-339", 7);
        g1.addChild(g11);
        GenreDto g2 = new GenreDto("Non-fiction", "genre-xyz-313", 5);
        GenreDto g3 = new GenreDto("Horror", "genre-uvw-999", 13);
        root.addChild(g1);
        root.addChild(g2);
        root.addChild(g3);
        return root;
    }
}
