package va.hagn.bookshop.genre;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.Genre;

import java.util.List;
import java.util.Map;

@Service
public class GenreService {
    private final GenreRepository repository;
    private final BooksCounter booksCounter;

    public GenreService(GenreRepository repository, BooksCounter orderedBooksCounter) {
        this.repository = repository;
        this.booksCounter = orderedBooksCounter;
    }

    public List<GenreDto> getGenresWithBookAmounts() {
        Genre tree = repository.getGenreTree();
        Map<Integer, Integer> booksCountPerGenre = repository.getBooksCountPerGenreMap();
        GenreDto root = booksCounter.computeGenreTree(tree, booksCountPerGenre);
        return root.getChildren();
    }

    public Page<Book> getPageOfBooksByGenre(String slug, int offset, int limit) {
        int pageNumber = offset / limit;
        Pageable nextPage = PageRequest.of(pageNumber, limit);

        return repository.findBooksByGenre(slug, nextPage);
    }

    public Genre getGenreBySlug(String slug) {
        return repository.findBySlug(slug);
    }
}
