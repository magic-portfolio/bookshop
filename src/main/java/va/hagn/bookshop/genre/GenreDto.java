package va.hagn.bookshop.genre;

import va.hagn.bookshop.data.Genre;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenreDto {
    private final String name;
    private final String slug;
    private final int amount;
    private final List<GenreDto> children = new ArrayList<>();

    public GenreDto(String name, String slug, int amount) {
        this.name = name;
        this.slug = slug;
        this.amount = amount;
    }

    public static GenreDto fromGenre(Genre genre, int amount) {
        return new GenreDto(genre.getName(), genre.getSlug(), amount);
    }

    public void addChild(GenreDto genre) {
        children.add(genre);
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public int getAmount() {
        return amount;
    }

    public List<GenreDto> getChildren() {
        return Collections.unmodifiableList(children);
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }
}
