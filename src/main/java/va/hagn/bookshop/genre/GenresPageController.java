package va.hagn.bookshop.genre;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.BooksPageDto;
import va.hagn.bookshop.data.Genre;
import va.hagn.bookshop.data.SearchWordDto;

@Controller
public class GenresPageController {

    private final GenreService genreService;

    public GenresPageController(GenreService genreService) {
        this.genreService = genreService;
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/genres")
    public String genresPage(Model model) {
        model.addAttribute("genres", genreService.getGenresWithBookAmounts());
        return "genres/index";
    }

    @GetMapping("/genres/{slug}")
    public String slugPage(@PathVariable String slug, Model model) {
        model.addAttribute("booksList", genreService.getPageOfBooksByGenre(slug, 0, 20).getContent());
        Genre genre = genreService.getGenreBySlug(slug);
        model.addAttribute("genre", genre);
        model.addAttribute("parent", genre.getParent());
        return "genres/slug";
    }

    @GetMapping("/books/genre/{slug}")
    @ResponseBody
    public BooksPageDto getBooksByGenre(@PathVariable String slug,
                                      @RequestParam Integer offset,
                                      @RequestParam Integer limit) {
        return new BooksPageDto(genreService.getPageOfBooksByGenre(slug, offset, limit).getContent());
    }
}
