package va.hagn.bookshop.genre;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import va.hagn.bookshop.data.Book;
import va.hagn.bookshop.data.Genre;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

    @Query("select g from Genre g join fetch g.children")
    List<Genre> findAllGenresWithChildren();

    default Genre getGenreTree() {
        Genre root = findAllGenresWithChildren().stream().filter(g -> g.getId() == 0).findAny().get();
        root.getChildren().removeIf(g -> g.getId() == 0);
        return root;
    }

    @Query("select g.id as id, count(b) as booksCount from Book b join b.genres g group by g")
    List<BooksCount> getBooksCountPerGenre();

    default Map<Integer, Integer> getBooksCountPerGenreMap() {
        return getBooksCountPerGenre().stream()
                .collect(Collectors.toMap(BooksCount::getId, BooksCount::getBooksCount));
    }

    @Query("select b from Book b join b.genres g where g.slug = :slug")
    Page<Book> findBooksByGenre(String slug, Pageable nextPage);

    Genre findBySlug(String slug);

    interface BooksCount {
        Integer getId();
        Integer getBooksCount();
    }
}
