package va.hagn.bookshop.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringdocConfig {
    @Bean
    public OpenAPI bookshopOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Bookshop API")
                        .description("API for bookstore")
                        .version("v1.0")
                        .license(new License()
                                .name("BSD 3-Clause License")
                                .url("https://opensource.org/license/bsd-3-clause/"))
                        .contact(new Contact()
                                .name("API owner")
                                .email("vaagn.khachatryan@gmail.com")
                                .url("http://www.ownersite.com")));
    }
}
