package va.hagn.bookshop.author;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import va.hagn.bookshop.data.Author;
import va.hagn.bookshop.data.BooksPageDto;
import va.hagn.bookshop.data.SearchWordDto;

import java.util.List;
import java.util.Map;

@Controller
@Tag(name = "authors data")
public class AuthorsController {

    private final AuthorService authorService;

    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @ModelAttribute("authorsMap")
    public Map<String, List<Author>> authorsMap() {
        return authorService.getAuthorsGroupedByLetter();
    }

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @GetMapping("/authors")
    public String authorsPage(){
        return "authors/index";
    }

    @GetMapping("/authors/{slug}")
    public String authorBioPage(@PathVariable String slug, Model model) {
        model.addAttribute("author", authorService.getAuthorBySlug(slug));
        model.addAttribute("booksAmount", authorService.getBooksAmountByAuthor(slug).toString());
        model.addAttribute("books", authorService.getPageOfBooksByAuthor(slug, 0, 6));
        return "authors/slug";
    }

    @GetMapping(value = "/books/author/{slug}", params = {})
    public String authorBooksPage(@PathVariable String slug, Model model) {
        model.addAttribute("author", authorService.getAuthorBySlug(slug));
        model.addAttribute("booksList", authorService.getPageOfBooksByAuthor(slug, 0, 20).getContent());
        return "books/author";
    }

    @GetMapping(value = "/books/author/{slug}", params = {"offset", "limit"})
    @ResponseBody
    public BooksPageDto getAuthorBooksPage(@PathVariable String slug,
                                           @RequestParam Integer offset,
                                           @RequestParam Integer limit) {
        return new BooksPageDto(authorService.getPageOfBooksByAuthor(slug, offset, limit).getContent());
    }

    @Operation(summary = "method to get map of authors")
    @GetMapping("/api/authors")
    @ResponseBody
    public Map<String,List<Author>> authors(){
        return authorService.getAuthorsGroupedByLetter();
    }
}
