package va.hagn.bookshop.author;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import va.hagn.bookshop.data.Author;
import va.hagn.bookshop.data.Book;

public interface AuthorRepository extends JpaRepository<Author, Integer> {
    Author findBySlug(String slug);

    @Query("select count(b) from Book b join b.author a where a.slug = :slug")
    Integer countBooksByAuthor(String slug);

    @Query("select b from Book b join b.author a where a.slug = :slug")
    Page<Book> findBooksByAuthor(String slug, Pageable page);
}
