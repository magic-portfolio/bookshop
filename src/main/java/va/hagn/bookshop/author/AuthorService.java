package va.hagn.bookshop.author;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import va.hagn.bookshop.data.Author;
import va.hagn.bookshop.data.Book;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    private final AuthorRepository repository;

    public AuthorService(AuthorRepository repository) {
        this.repository = repository;
    }

    public Map<String, List<Author>> getAuthorsGroupedByLetter() {
        return repository.findAll().stream()
                .sorted(Comparator.comparing(Author::getLastName))
                .collect(Collectors.groupingBy(
                        (a -> a.getLastName().substring(0, 1)),
                        LinkedHashMap::new,  // iteration will be in the sorted order
                        Collectors.toList()));
    }

    public Author getAuthorBySlug(String slug) {
        return repository.findBySlug(slug);
    }

    public Integer getBooksAmountByAuthor(String slug) {
        return repository.countBooksByAuthor(slug);
    }

    public Page<Book> getPageOfBooksByAuthor(String slug, int offset, int limit) {
        int pageNumber = offset / limit;
        Pageable nextPage = PageRequest.of(pageNumber, limit);
        return repository.findBooksByAuthor(slug, nextPage);
    }
}
