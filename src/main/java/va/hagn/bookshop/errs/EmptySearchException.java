package va.hagn.bookshop.errs;

public class EmptySearchException extends RuntimeException {
    public EmptySearchException(String message) {
        super(message);
    }
}
