package va.hagn.bookshop.errs;

public class BookstoreApiWrongParameterException extends RuntimeException {
    public BookstoreApiWrongParameterException(String message) {
        super(message);
    }
}
