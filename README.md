# Bookshop web app

## Local execution
To start and stop the db locally run the following commands. PgAdmin url is http://localhost:5050

```bash
docker compose -f src/main/resources/compose.yaml up -d
```

```bash
docker compose -f src/main/resources/compose.yaml down
```

## Liquibase

To get an initial xml run

```bash
./mvnw -e clean package liquibase:diff -DskipTests=true
```

## Open API / Swagger

[JSON](http://localhost:8080/v3/api-docs/)
[Swagger UI](http://localhost:8080/swagger-ui/index.html)

## WebDriver

[Chrome driver](https://chromedriver.chromium.org/downloads) for local execution.
```bash
unzip -u Downloads/chromedriver_linux64.zip -d /opt/chromedriver/
```

[FireFox driver](https://github.com/mozilla/geckodriver/releases) for local execution.

[Selenium Grid](https://github.com/SeleniumHQ/docker-selenium) docker images for running in CI.

## GitLab
### Local gitlab runner
First, register the runner with GitLab instance:
```bash
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

Then modify the configuration in `/srv/gitlab-runner/config/config.toml` following the [testcontainers guide](https://www.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/).

Finally, start the runner:

```bash
docker run -d --name gitlab-runner --restart always \
-v /srv/gitlab-runner/config:/etc/gitlab-runner \
-v $XDG_RUNTIME_DIR/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
```

### Running gitlab ci locally

```bash
docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker --docker-volumes /var/run/docker.sock:/var/run/docker.sock 
```

nsenter -U --preserve-credentials -n -m -t $(cat $XDG_RUNTIME_DIR/docker.pid)

### Shared folder for testing file downloads

Based on ideas in
* https://medium.com/@nielssj/docker-volumes-and-file-system-permissions-772c1aee23ca
* https://stackoverflow.com/a/71087476

By analyzing /etc/passwd and /etc/group of the selenium/standalone-chrome image, one can see that it runs its services
as seluser which has id 1200 and group seluser with id 1201.

```bash
mkdir -p /var/tmp/bookshop
sudo chown 101199:101200 /var/tmp/bookshop
```
